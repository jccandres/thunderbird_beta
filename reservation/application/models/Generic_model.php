<?php

class Generic_model extends CI_model
{
	  public function __construct()
    {
    	parent::__construct();
    }

    public function getColumnValue($column, $table, $where)
    {
      $this->db->select($column);
      $this->db->from($table);
      $this->db->where($where);
      $result = $this->db->get();
      return $result->result()[0]->$column;
    }

}
