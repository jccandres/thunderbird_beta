<?php

class Inclusion_model extends CI_model {
	public function __construct()
    {
    	parent::__construct();
    }

    public function getInclusions()
    {
    	$query = $this->db->get('inclusions');
    	return $query->result();
    }

    public function addInclusion($data)
    {
    	return $this->db->insert('inclusions', $data);
    }

    public function updateInclusion($data)
    {
    	return $this->db->update('inclusions', $data, array('inclusion_id' => $data['inclusion_id']));
    }

    public function deleteInclusion($id)
    {
			#delete inclusions by inclusion id in room type and inclusion third table
			$this->db->delete("room_type_inclusions", array('inclusion_id' => $id));
    	return $this->db->delete("inclusions", array('inclusion_id' => $id));
    }

		public function deleteInclusionByInclusion($id) {
    	return $this->db->delete("inclusions", array('inclusion_id' => $id));
    }

    public function getInclusionsByRoomType($room_type_id)
    {
    	$this->db->select('*');
    	$this->db->from('inclusions');
    	$this->db->join('room_type_inclusions', 'room_type_inclusions.inclusion_id = inclusions.inclusion_id');
    	$this->db->where('room_type_id', $room_type_id);
    	$query = $this->db->get();
    	return $query->result();
    }
}
