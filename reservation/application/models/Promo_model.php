<?php

class Promo_model extends CI_model
{
	  public function __construct()
    {
    	parent::__construct();
    }

    public function addPromo($data)
    {
      if(isset($data['max_lead_time'])) {
        $data['max_lead_time'] = $data['max_lead_time'] * 24;
      }

      if(isset($data['start_date'])) {
        $data['start_date'] = date("Y-m-d H:i:s", strtotime($data['start_date']));
      }

      if(isset($data['end_date'])) {
        $data['end_date'] = date("Y-m-d H:i:s", strtotime($data['end_date']));
      }

      if(isset($data['rate_plan_id']))
      {
        $data['rate_plan_id'] = "*".implode("*,*", $data['rate_plan_id'])."*";
      }

    	$this->db->insert('promos', $data);
      return $this->db->insert_id();
    }

    public function getPromos()
    {
      $this->load->model('rates_model');
      $final = array();
      $query = $this->db->get('promos');
      $result = $query->result();
      foreach ($result as $key => $promo) {
        $promo->date_range = date("m/d/Y", strtotime(date($promo->start_date)))." to ".date("m/d/Y", strtotime(date($promo->end_date)));
        $promo->max_lead_time = $promo->max_lead_time * 24;
        $promo->max_rooms = $promo->max_rooms == null ? "No Max" : $promo->max_rooms;
        $promo->max_nights = $promo->max_nights == null ? "No Max" : $promo->max_nights;
        $rate_plans = explode(",", $promo->rate_plan_id);
        $promo->rate_plan = "";
        foreach ($rate_plans as $key => $id) {
          $promo->rate_plan .= "- ".$this->rates_model->getRateNameById(str_replace("*", "", $id))."<br>";
        }
      }
      return $result;
    }
    
    public function editPromo($data)
    {
      $this->db->where("promo_id = ".$data['promo_id']);
      unset($data['promo_id']);

      if(isset($data['max_lead_time'])) {
        $data['max_lead_time'] = $data['max_lead_time'] * 24;
      }

      if(isset($data['start_date'])) {
        $data['start_date'] = date("Y-m-d H:i:s", strtotime($data['start_date']));
      }

      if(isset($data['end_date'])) {
        $data['end_date'] = date("Y-m-d H:i:s", strtotime($data['end_date']));
      }

      if(isset($data['rate_plan_id']))
      {
        $data['rate_plan_id'] = "*".implode("*,*", $data['rate_plan_id'])."*";
      }

      return $this->db->update("promos", $data);
    }
}
?>
