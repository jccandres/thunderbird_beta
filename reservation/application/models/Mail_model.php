<?php

class Mail_model extends CI_model {
  public function saveMail($data)
  {
    return $this->db->insert('emails', $data);
  }

  public function getMails()
  {
    $query = $this->db->get('emails');
    return $query->result();
  }
}
