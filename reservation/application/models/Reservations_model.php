<?php

class Reservations_model extends CI_model
{
	  public function __construct()
    {
    	parent::__construct();
    }

    public function addPromo($data)
    {
      if(isset($data['max_lead_time'])) {
        $data['max_lead_time'] = $data['max_lead_time'] * 24;
      }

      if(isset($data['start_date'])) {
        $data['start_date'] = date("Y-m-d H:i:s", strtotime($data['start_date']));
      }

      if(isset($data['end_date'])) {
        $data['end_date'] = date("Y-m-d H:i:s", strtotime($data['end_date']));
      }

      if(isset($data['rate_plan_id']))
      {
        $data['rate_plan_id'] = "*".implode("*,*", $data['rate_plan_id'])."*";
      }

    	$this->db->insert('promos', $data);
      return $this->db->insert_id();
    }

    public function getReservations()
    {
      $this->load->model("generic_model");

      $this->db->from("reserved_rooms");
      $this->db->join("room_type", 'reserved_rooms.room_type_id = room_type.room_type_id');
      $this->db->join("guests", "reserved_rooms.guest_id = guests.guest_id");
      $this->db->order_by("reserved_rooms.date_in", "desc");
      $query = $this->db->get();
      $result = $query->result();
      foreach ($result as $key => $reserve) {
        $reserve->date_out = date("m/d/Y", strtotime($reserve->date_out));
        $reserve->date_in = date("m/d/Y", strtotime($reserve->date_in));

        $promos = explode(",", $reserve->promo_id);
        $reserve->promo_name = "";
        foreach($promos as $p_k => $p_val) {
          $p_val = str_replace("*", "", $p_val);
          $reserve->promo_name .= "- ".$this->generic_model->getColumnValue("name", "promos", "promo_id = $p_val")."<br>"; # prameters "column name", "table", "where clause"
        }

        $reserve->rate_name = $this->generic_model->getColumnValue("name", "rate_plan", "rate_plan_id = ".$reserve->rate_plan_id);
        // var_dump($reserve->promo_name); die();
        $reserve->guest_name = $reserve->fname." ".$reserve->lname;
      }
      return $result;
    }

    public function editPromo($data)
    {
      $this->db->where("promo_id = ".$data['promo_id']);
      unset($data['promo_id']);

      if(isset($data['max_lead_time'])) {
        $data['max_lead_time'] = $data['max_lead_time'] * 24;
      }

      if(isset($data['start_date'])) {
        $data['start_date'] = date("Y-m-d H:i:s", strtotime($data['start_date']));
      }

      if(isset($data['end_date'])) {
        $data['end_date'] = date("Y-m-d H:i:s", strtotime($data['end_date']));
      }

      if(isset($data['rate_plan_id']))
      {
        $data['rate_plan_id'] = "*".implode("*,*", $data['rate_plan_id'])."*";
      }

      return $this->db->update("promos", $data);
    }
}
?>
