<?php

class Rates_model extends CI_model
{
	  public function __construct()
    {
    	parent::__construct();
    }

    public function getRateNameById($id)
    {
      $this->db->select("name");
      $this->db->from("rate_plan");
      $this->db->where("rate_plan_id = $id");
      $result = $this->db->get();
      return $result->result()[0]->name;
    }

    public function addRatePlan($data)
    {
      if(isset($data['lead_time_max'])) {
        $data['lead_time_max'] = $data['lead_time_max'] * 24;
      }
    	$this->db->insert('rate_plan', $data);
      return $this->db->insert_id();
    }

    public function editRatePlan($data)
    {
      if(isset($data['lead_time_max'])) {
        $data['lead_time_max'] = $data['lead_time_max'] * 24;
      }

      $this->db->where("rate_plan_id = ".$data['rate_plan_id']);
      unset($data['rate_plan_id']);
      return $this->db->update("rate_plan", $data);
    }

    public function getRates()
    {
      $final = array();
      $query = $this->db->get('rate_plan');
      $result = $query->result();
      foreach ($result as $key => $plan) {
        # Rate Details
        $result[$key]->rate_details = "No discount";
        if($plan->discount_rate != 0){
          $result[$key]->rate_details = "Less ".$plan->discount_rate."%";
          if($plan->discount_type == 2) {
            $result[$key]->rate_details = "Rate increase by ".$plan->discount_rate."%";
          }
        }

        # validity
        $result[$key]->validity = $plan->validity == 1 ? "All Year Round" : "Holidays, Holyweeks, etc.";

        # conditions
        $discount_type = "decrease";
        if($plan->discount_type == 2) {
          $discount_type = "increase";
        }
        $result[$key]->conditions = array();
        if($plan->lead_time_max != 0 && $plan->lead_time_max != null) {
          $result[$key]->conditions[] =  "- ".$plan->lead_time_max." hours Lead Time";
        }
        if($plan->occupancy_percent != 0 && $plan->occupancy_percent != null) {
          $result[$key]->conditions[] =  "- ".$plan->occupancy_percent."% Occupancy";
        }
        if($plan->discount_rate != 0 && $plan->discount_type != null) {
          $result[$key]->conditions[] =  "- rate $discount_type by ".$plan->discount_rate."%";
        }

        // if($plan->nights_max == 0) {
        //   $plan->nights_max = "No limit";
        // }
      }
      return $result;
    }
}
?>
