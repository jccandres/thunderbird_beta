<?php

class Room_model extends CI_model {
	public function __construct()
    {
    	parent::__construct();
        $this->load->model('inclusion_model');
    }

    public function getRooms($room_type_id)
    {
    	$this->db->where("room_type_id = $room_type_id");
    	$query = $this->db->get('rooms');
    	return $query->result();
    }

    public function addRoom($room_type_id)
    {
    	$data['room_type_id'] = $room_type_id;
    	$this->db->insert('rooms', $data);
    	return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function updateRoom($room_id, $room_num = null)
    {
    	$this->db->set('room_number', $room_num);
    	$this->db->where('room_id', $room_id);
    	return $this->db->update('rooms');
    }

    public function deleteRoom($room_id)
    {
    	$data['room_id'] = $room_id;
    	$this->db->delete('rooms', $data);
    	return ($this->db->affected_rows() != 1) ? false : true;
    }

    public function getRoomTypes()
    {
        $query = $this->db->get('room_type');
        $result = $query->result();
        foreach ($result as $key => $value) {
            $result[$key]->inclusions = $this->inclusion_model->getInclusionsByRoomType($value->room_type_id);
        }
        return $result;
    }

		/*
		 * Get room type by id
		 */
		public function getRoomType($room_type_id)
		{
			$this->db->where("room_type_id = $room_type_id");
    	$query = $this->db->get('room_type');
    	return $query->result();
		}

    public function addRoomType($data)
    {
        if (@$data['inclusions']) {
            $inclusions = $data['inclusions'];
            unset($data['inclusions']);
        }
        $this->db->insert('room_type', $data);
        $last_insert_id = $this->db->insert_id();
        if (@$inclusions) {
            foreach ($inclusions as $key => $value) {
                $this->addInclusionByRoomType($last_insert_id, $value);
            }
        }
        else {
            return $last_insert_id;
        }
    }

    public function addInclusionByRoomType($room_type_id, $inclusion_id)
    {
        $this->db->insert('room_type_inclusions', array('room_type_id' =>  $room_type_id, 'inclusion_id' => $inclusion_id));
    }

    public function updateRoomType($data)
    {
				#delete inclusion by room type id
				$this->inclusion_model->deleteInclusionByRoomType($data['room_type_id']);

				if (@$data['inclusions']) {
					$inclusions = $data['inclusions'];
					foreach ($inclusions as $key => $value) {
						$this->addInclusionByRoomType($data['room_type_id'], $value);
					}
					unset($data['inclusions']);
				}

        return $this->db->update('room_type', $data, array('room_type_id' => $data['room_type_id']));
    }

    public function deleteRoomType($id)
    {
        if ($this->deleteRoomsByType($id)) {
            return $this->db->delete("room_type", array('room_type_id' => $id));
        }
        else {
            return false;
        }

    }

    public function deleteRoomsByType($id) #room_type_id
    {
        return $this->db->delete("rooms", array('room_type_id' => $id));
    }
}
?>
