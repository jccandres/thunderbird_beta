<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class admin extends CI_Controller {

	public function __construct()
	{
        parent::__construct();
        $this->load->model('reservation_model');
        $this->load->model('room_model');
        $this->load->model('inclusion_model');
				$this->load->model('mail_model');
				$this->load->model('rates_model');
				$this->load->model('promo_model');
				$this->load->model('bookings_model');

				$this->load->library('pagination');
    }

	public function wrapper($body, $data = NULL)
	{
		if (isset($this->session->userdata['logged_in'])) {
			$this->load->view('partials/header');
			$this->load->view('partials/left-sidebar');
			$this->load->view($body, $data);
			// $this->load->view('partials/right-sidebar');
			$this->load->view('partials/footer');
		}
		else {
			redirect(base_url('admin'));
		}
	}

	public function index()
	{
		if (isset($this->session->userdata['logged_in'])) {
			// $this->mails();
			redirect(base_url('admin/mails'));
		}
		else {
			$this->load->view('admin/login');
		}
	}

	public function login()
	{
		$this->session->userdata['logged_in'] = 1;
		redirect(base_url('admin'));
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect(base_url('admin'));
	}

	public function dashboard()
	{
		$this->wrapper('admin/dashboard');
	}

/* Rooms Start*/
	public function rooms()
	{
		if (isset($_GET['date'])) {
			$data['date'] = $_GET['date'];
		}
		else {
			$data['date'] = date("d-m-Y");
		}

		/* Get available rooms for 10 days */
		for ($i=0; $i < 10; $i++) {
			$date = date("d-m-Y", strtotime("+$i days", strtotime($data['date'])));
			$available_rooms = $this->reservation_model->getAvailableRooms($date, $date);

			foreach ($available_rooms as $value) {
				$data['rooms'][$value->name][] = $value->num_room;
			}
		}
		/* Get available rooms for 10 days */

		// $data['room_type'] = $this->reservation_model->getRooms();
		$data['room_type'] = $this->room_model->getRoomTypes();

		// var_dump($data); die();

		$this->wrapper('admin/rooms', $data);
	}

	public function getRooms($room_type_id)
	{
		echo json_encode($this->room_model->getRooms($room_type_id));
	}

	public function addRoom($room_type_id)
	{
		if ($this->room_model->addRoom($room_type_id)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}

	public function updateRoom($room_type_id, $room_id, $room_num)
	{
		if ($this->room_model->updateRoom($room_id, $room_num)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}

	public function deleteRoom($room_type_id, $room_id)
	{
		if ($this->room_model->deleteRoom($room_id)) {
			echo json_encode($this->room_model->getRooms($room_type_id));
		}
	}
/* Rooms END */

/* Room Types START */
	public function roomTypes()
	{
		$data['roomTypes'] = $this->room_model->getRoomTypes();
		$data['inclusions'] = $this->inclusion_model->getInclusions();
		if (isset($_GET['dev'])) {
			$data['dev'] = true;
		}
		$this->wrapper('admin/room_types', $data);
	}

	/*
	 * Edit room type page
	 */
	public function roomType($room_type_id)
	{
		$data['roomType'] = $this->room_model->getRoomType($room_type_id);
		$data['inclusions'] = $this->inclusion_model->getInclusions($room_type_id);
		$data['selected_inclusions'] = $this->inclusion_model->getInclusionsByRoomType($room_type_id);
		$this->wrapper('admin/edit_room_type', $data);
	}

	public function addRoomType()
	{
		$data = $this->input->post();
		// var_dump($data); die();
		if ($this->room_model->addRoomType($data)) {
			$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Add Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}

	public function updateRoomType()
	{
		$data = $this->input->post();
		// var_dump($data); die();
		if ($this->room_model->updateRoomType($data)) {
			$msg_data = array('alert_msg' => 'Edit Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Edit Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}

	public function deleteRoomType($id)
	{
		if ($this->room_model->deleteRoomType($id)) {
			$msg_data = array('alert_msg' => 'Delete Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Delete Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/roomTypes'));
	}
/* Room Types END */

/* Inclusions START */
	public function inclusions()
	{
		$data['inclusions'] = $this->inclusion_model->getInclusions();
		if (isset($_GET['dev'])) {
			$data['dev'] = true;
		}
		$this->wrapper('admin/inclusions', $data);
	}

	public function addInclusion()
	{
		$data = $this->input->post();
		if ($this->inclusion_model->addInclusion($data)) {
			$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Add Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}

	public function editInclusion()
	{
		$data = $this->input->post();
		if ($this->inclusion_model->updateInclusion($data)) {
			$msg_data = array('alert_msg' => 'Update Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Update Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}

	public function deleteInclusion($id)
	{
		if ($this->inclusion_model->deleteInclusion($id)) {
			$msg_data = array('alert_msg' => 'Delete Success', 'alert_color' => 'green');
		}
		else {
			$msg_data = array('alert_msg' => 'Delete Fail', 'alert_color' => 'red');
		}
		$this->session->set_flashdata($msg_data);
		redirect(base_url('admin/inclusions'));
	}
/* Inclusions END */


/* Rate Plans START  */
public function rates()
{
	$data['ratePlan'] = $this->rates_model->getRates();
	if (isset($_GET['dev'])) {
		$data['dev'] = true;
	}
	$this->wrapper("admin/rate_plans", $data);
}

public function addRatePlan()
{
	$data = $this->input->post();
	if ($this->rates_model->addRatePlan($data)) {
		$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
	}
	else {
		$msg_data = array('alert_msg' => 'There was a problem in adding a rate plan. Please try again later.', 'alert_color' => 'red');
	}
	$this->session->set_flashdata($msg_data);
	redirect(base_url('admin/rates'));
}

public function editRatePlan()
{
	$data = $this->input->post();
	if($this->rates_model->editRatePlan($data)) {
		$msg_data = array('alert_msg' => 'Edit Success', 'alert_color' => 'green');
	}
	else {
		$msg_data = array('alert_msg' => 'There was a problem in editing the rate plan. Please try again later.', 'alert_color' => 'red');
	}
	$this->session->set_flashdata($msg_data);
	redirect(base_url('admin/rates'));
}
/* Rate Plans END  */


/* Promos START */
public function promos()
{
	$data['promos'] = $this->promo_model->getPromos();
	$data['rate_plans'] = $this->rates_model->getRates();
	if (isset($_GET['dev'])) {
		$data['dev'] = true;
	}
	$this->wrapper("admin/promos", $data);
}

public function addPromo()
{
	$data = $this->input->post();
	if ($this->promo_model->addPromo($data)) {
		$msg_data = array('alert_msg' => 'Add Success', 'alert_color' => 'green');
	}
	else {
		$msg_data = array('alert_msg' => 'There was a problem in adding a promo. Please try again later.', 'alert_color' => 'red');
	}
	$this->session->set_flashdata($msg_data);
	redirect(base_url('admin/promos'));
}

public function editPromo()
{
	$data = $this->input->post();
	if($this->promo_model->editPromo($data)) {
		$msg_data = array('alert_msg' => 'Edit Success', 'alert_color' => 'green');
	}
	else {
		$msg_data = array('alert_msg' => 'There was a problem in editing the promo plan. Please try again later.', 'alert_color' => 'red');
	}
	$this->session->set_flashdata($msg_data);
	redirect(base_url('admin/promos'));
}
/* Promos END */

/* Reservations START */
public function reservations()
{
	# Get Pagination
	$pag_conf['base_url'] = base_url()."admin/reservations";
	$pag_conf['reuse_query_string'] = TRUE;	# maintain get varibles if any
	$pag_conf['total_rows'] = $this->bookings_model->getTotalBookings();
	$pag_conf['per_page'] = TABLE_DATA_PER_PAGE;
	$this->pagination->initialize($pag_conf);
	$data["pagination"] = $this->pagination->create_links();

	$data['reservations'] = $this->bookings_model->getBookings();
	$data['room_types'] = $this->room_model->getRoomTypes();

	if (isset($_GET['dev'])) {
		$data['dev'] = true;
	}
	$this->wrapper("admin/reservations", $data);
}
/* Reservations END */





/* Mails */
	public function mails()
	{
		$data['mails'] = $this->mail_model->getMails();
		$this->wrapper('admin/mails', $data);
	}
/* Mails END */

}
