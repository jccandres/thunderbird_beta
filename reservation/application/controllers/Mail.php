<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class mail extends CI_Controller {

  public function __construct()
  {
    parent::__construct();
    $this->load->model('mail_model');

    $this->load->library('email');
    $config_mail['protocol']='smtp';
    $config_mail['smtp_host']='mail.smtp2go.com';
    $config_mail['smtp_port']='80';
    $config_mail['smtp_timeout']='30';
    $config_mail['smtp_user']='betamail@optimindsolutions.com';
    $config_mail['smtp_pass']='MDFldDJ5a3lkbWk3';
    $config_mail['charset']='utf-8';
    $config_mail['newline']="\r\n";
    $config_mail['wordwrap'] = TRUE;
    $config_mail['mailtype'] = 'html';
    $this->email->initialize($config_mail);
  }

  public function index()
  {

  }

  public function send()
  {
    $post = $this->input->post();
    $site_url = explode("/reservation", base_url())[0];
    $isSent = 0;

    # save to db
    $mailSaved = $this->mail_model->saveMail($post);
    if ($mailSaved) {
      $isSent = 1;
    }

    # send to Email
    $this->email->from('noreply@thunderbird.com', 'Thunderbird');
    $this->email->to('jcandres@myoptimind.com');
    $this->email->bcc('cvalerio@myoptimind.com');
    $this->email->subject('Inquiry - ' .  $post['name']);
    $msg = "
    <table>
      <tr><td>Name: " . $post['title'] . " " . $post['name'] . "</td></tr>
      <tr><td>Email: " . $post['email'] . "</td></tr>
      <tr><td>Contact No.: " . $post['contact'] . "</td></tr>
      <tr><td>Facility: " . $post['facility'] . "</td></tr>
      <tr><td>Message: " . $post['message'] . "</td></tr>
    </table>
    ";
    $this->email->message($msg);
    if ($this->email->send())
    {
      $isSent = 1;
    }
    else {
      $isSent = 0;
    }

    redirect($site_url . "/contact-us/?sent=" . $isSent);
  }
}
