
<!--sidebar start-->
<aside>
  <div id="sidebar"  class="nav-collapse ">
    <!-- sidebar menu start-->
    <ul class="sidebar-menu" id="nav-accordion">
      <?php
      if (isset($_GET['dev']) || uri_string() != "admin/mails") {
      ?>
      <li>
        <a href="<?php echo base_url('admin/dashboard'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Dashboard</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/rooms'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Rooms</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/roomTypes'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Room Types</span>
        </a>
      </li>

      <li>
        <a href="<?php echo base_url('admin/inclusions'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Inclusions</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url('admin/rates'); ?>" <?php echo uri_string() == "admin/rates" ? "class='active'" : ""; ?>>
          <i class="fa fa-book"></i>
          <span>Rate Plans</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url('admin/promos'); ?>" <?php echo uri_string() == "admin/promos" ? "class='active'" : ""; ?>>
          <i class="fa fa-tags"></i>
          <span>Promos</span>
        </a>
      </li>
      <li>
        <a href="<?php echo base_url('admin/reservations'); ?>" <?php echo uri_string() == "admin/reservations" ? "class='active'" : ""; ?>>
          <i class="fa fa-book"></i>
          <span>Reservations</span>
        </a>
      </li>

      <?php
      }
      ?>

      <li>
        <a href="<?php echo base_url('admin/mails'); ?>">
          <i class="fa fa-dashboard"></i>
          <span>Mails</span>
        </a>
      </li>

    </ul>
    <!-- sidebar menu end-->
  </div>
</aside>
<!--sidebar end-->
