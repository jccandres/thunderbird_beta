<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="Mosaddek">
  <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
  <link rel="shortcut icon" href="img/favicon.png">

  <title>Thunderbird - admin</title>

  <!-- Bootstrap core CSS -->
  <link href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-reset.css'); ?>" rel="stylesheet">
  <!--external css-->
  <link href="<?php echo base_url('assets/assets/font-awesome/css/font-awesome.css'); ?>" rel="stylesheet" />

  <!--right slidebar-->
  <link href="<?php echo base_url('assets/css/slidebars.css'); ?>" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="<?php echo base_url('assets/css/style.css'); ?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/style-responsive.css'); ?>" rel="stylesheet" />

  <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
  <!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
  <![endif]-->

  <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/assets/bootstrap-datepicker/css/datepicker.css'); ?>" />
  <script src="<?php echo base_url('assets/js/jquery.js'); ?>"></script>
</head>

<body>

  <section id="container" class="">
    <!--header start-->
    <header class="header white-bg">
      <div class="sidebar-toggle-box">
        <i class="fa fa-bars"></i>
      </div>
      <!--logo start-->
      <a href="" class="logo" >Thunderbird</a>
      <!--logo end-->

      <div class="top-nav ">
        <ul class="nav pull-right top-menu">
          <li>
            <input type="text" class="form-control search" placeholder="Search">
          </li>
          <!-- user login dropdown start-->
          <li class="dropdown">
            <a data-toggle="dropdown" class="dropdown-toggle" href="#">
              <!-- <img alt="" src="img/avatar1_small.jpg"> -->
              <span class="username">Admin</span>
              <b class="caret"></b>
            </a>
            <ul class="dropdown-menu extended logout">
              <div class="log-arrow-up"></div>
              <!-- <li><a href="#"><i class=" fa fa-suitcase"></i>Profile</a></li> -->
              <!-- <li><a href="#"><i class="fa fa-cog"></i> Settings</a></li> -->
              <!-- <li><a href="#"><i class="fa fa-bell-o"></i> Notification</a></li> -->
              <li><a href="<?php echo base_url('admin/logout'); ?>"><i class="fa fa-key"></i> Log Out</a></li>
            </ul>
          </li>

          <!-- user login dropdown end -->
          <!-- <li class="sb-toggle-right">
            <i class="fa  fa-align-right"></i>
          </li> -->
        </ul>
      </div>
    </header>
    <!--header end-->
