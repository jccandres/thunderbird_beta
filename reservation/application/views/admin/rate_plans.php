<?php
// var_dump($roomTypes); die();
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Rate Plans
            </header>

            <div class="panel-body">
            	<div class="col-md-3 mbot15">
                  <a href="#AddRatePlan" data-toggle="modal" class="btn btn-info">Add</a>
              </div>
              <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                  <?php echo $this->session->flashdata('alert_msg'); ?>
              </span>
            	<div class="table-responsive col-md-12">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Name</th>
            					<th>Description</th>
                      <th>Rate Details</th>
            					<th>Validity</th>
                      <th width="20%">Booking Condition</th>
                      <!-- <th>Check in/out Nights (min - max)</th> -->
                      <th>Meal Inclusion</th>
                      <th>Meal Description</th>
                      <th>Actions</th>
            				</tr>
            			</thead>
            			<tbody>
            				<?php
                    if(count($ratePlan) > 0) :
              				foreach ($ratePlan as $plan) { ?>
              				<tr>
              					<td><?php echo $plan->name; ?></td>
  	            				<td><?php echo $plan->description; ?></td>
  	            				<td><?php echo $plan->rate_details; ?></td>
  	            				<td><?php echo $plan->validity; ?></td>
  	            				<td>
                          <?php
                          if(count($plan->conditions) > 0) {
                            echo implode("<br>", $plan->conditions);
                          } else {
                            echo "No booking conditions.";
                          }
                          ?>
                        </td>
                        <!-- <td><?php// echo $plan->nights_min." - ".$plan->nights_max." Nights"; ?></td> -->
                        <td><?php echo $plan->meal_inclusion; ?></td>
                        <td><?php echo $plan->meal_description != "" ? $plan->meal_description : "No meal description"; ?></td>
                        <td>
                          <!-- <a href="<?php //echo base_url('admin/roomType/') . $plan->rate_plan_id; ?>"><i class='fa fa-pencil'></i></a> -->
                          <button class="btn btn-xs btn-info edit-rate-plan" title="edit" href="#EditRatePlan"
                                  data-toggle="modal"
                                  data-id="<?php echo $plan->rate_plan_id; ?>"
                                  data-name="<?php echo $plan->name; ?>"
                                  data-description="<?php echo $plan->description; ?>"
                                  data-validity="<?php echo $plan->validity; ?>"
                                  data-lead_time_max="<?php echo $plan->lead_time_max/24; ?>"
                                  data-occupancy_percent="<?php echo $plan->occupancy_percent; ?>"
                                  data-discount_rate="<?php echo $plan->discount_rate; ?>"
                                  data-discount_type="<?php echo $plan->discount_type; ?>"
                                  data-meal_inclusion="<?php echo $plan->meal_inclusion; ?>"
                                  data-meal_description="<?php echo $plan->meal_description; ?>"
                                  >
                                  <i class="fa fa-pencil"></i>
                          </button>
                          <button class='btn btn-xs btn-danger' title="Delete" onclick="deleteRoomType(<?php echo $plan->rate_plan_id; ?>)"><i class='fa fa-times'></i></button>
                        </td>
  	            			</tr>
              				<?php
              				}
                    else: ?>
                    <tr>
                      <td colspan="8"><center>No items found. Add now by clicking the "Add" button.</center></td>
                    </tr>

                    <?php
                    endif;
              				?>
            			</tbody>
            		</table>
    			</div>
            </div>
        </section>
	</div>
</div>


		<!-- page end-->
	</section>
</section>
<!--main content end-->

<!-- Modals Start -->

	<!-- Add Rate Plan Modal -->
    <div class="modal fade " id="AddRatePlan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/addRatePlan'); ?>">
            	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Rate Plan</h4>
                </div>

                <div class="modal-body">

                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" class="form-control">
                	</div>

                	<div class="form-group">
                		<label>Description</label>
                		<textarea class="form-control" name="description"></textarea>
                	</div>

                  <div class="form-group">
                    <label>Validity</label>
                    <select class="form-control" name="validity" id="validity">
                      <option value="1">All Year Round</option>
                      <option value="2">Specific Dates (Holidays, Holyweek, etc)</option>
                    </select>
                  </div>

                  <!-- <div class="form-group">
                    <div class="row">
                      <label class="col-sm-4 control-label">Check-in Nights (Min - Max)</label>
                      <div class="col-sm-3">
                          <input type="number" class="form-control" name="nights_min">
                      </div>
                      <div class="col-sm-1">
                        -
                      </div>
                      <div class="col-sm-3">
                        <input type="number" class="form-control" name="nights_max">
                      </div>
                    </div>
                  </div> -->

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Lead Time</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="lead_time_max" class="form-control">
                          <span class="input-group-addon">days</span>
                        </div>
                      </div>
                      <!-- <div class="col-sm-1"> -->
                      <!-- </div> -->
                      <div class="col-sm-5">
                        <label>Occupancy Percentage</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="occupancy_percent" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Discount Percent</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="discount_rate" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                      <!-- <div class="col-sm-1"> -->
                      <!-- </div> -->
                      <div class="col-sm-5">
                        <label>Discount Type</label>
                        <select class="form-control" name="discount_type">
                          <option value="1">Decrease</option>
                          <option value="2">Increase</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Meal Inclusion</label>
                        <select class="form-control" name="meal_inclusion">
                          <option>Breakfast</option>
                          <option>Lunch</option>
                          <option>Dinner</option>
                          <option>No Meal Inclusion</option>
                        </select>
                      </div>
                      <div class="col-sm-7">
                        <label>Meal Description</label>
                        <textarea class="form-control" name="meal_description"></textarea>
                      </div>
                    </div>
                  </div>


                </div>
                <div class="modal-footer">
                    <input type="submit" name="" value="Add" class="btn btn-success">
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Add Rate Plan End -->

    <!-- Edit Rate Plan Modal -->
    <div class="modal fade " id="EditRatePlan" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/editRatePlan'); ?>">
            	<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Room Type</h4>
                </div>

                <div class="modal-body">

                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" id="edit_name" class="form-control">
                	</div>

                	<div class="form-group">
                		<label>Description</label>
                		<textarea class="form-control" name="description" id="edit_description"></textarea>
                	</div>

                  <div class="form-group">
                    <label>Validity</label>
                    <select class="form-control" name="validity" id="edit_validity">
                      <option value="1">All Year Round</option>
                      <option value="2">Specific Dates (Holidays, Holyweek, etc)</option>
                    </select>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Lead Time</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="lead_time_max" id="edit_lead_time_max" class="form-control">
                          <span class="input-group-addon">days</span>
                        </div>
                      </div>
                      <!-- <div class="col-sm-1"> -->
                      <!-- </div> -->
                      <div class="col-sm-5">
                        <label>Occupancy Percentage</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="occupancy_percent" id="edit_occupancy_percent" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Discount Percent</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="discount_rate" id="edit_discount_rate" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                      <!-- <div class="col-sm-1"> -->
                      <!-- </div> -->
                      <div class="col-sm-5">
                        <label>Discount Type</label>
                        <select class="form-control" name="discount_type" id="edit_discount_rate">
                          <option value="1">Decrease</option>
                          <option value="2">Increase</option>
                        </select>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-5">
                        <label>Meal Inclusion</label>
                        <select class="form-control" name="meal_inclusion" id="edit_meal_inclusion">
                          <option>Breakfast</option>
                          <option>Lunch</option>
                          <option>Dinner</option>
                          <option>No Meal Inclusion</option>
                        </select>
                      </div>
                      <div class="col-sm-7">
                        <label>Meal Description</label>
                        <textarea class="form-control" name="meal_description" id="edit_meal_description"></textarea>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="modal-footer">
           			    <input type="hidden" name="rate_plan_id" id="edit_rate_plan_id">
                    <input type="submit" name="" value="Edit" class="btn btn-success">
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Edit Rate Plan End -->

<!-- Modals END -->

<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";

    function deleteRoomType(id)
    {
        if (confirm("Are you sure you want to do this?")) {
            window.location = base_url + "/admin/deleteRoomType/" + id
        }
    }

	$(function(){
        $(".edit-rate-plan").on("click", function(){
          $("#edit_name").val($(this).data("name"));
          $("#edit_description").val($(this).data("description"));
          $("#edit_validity option[value='"+$(this).data("validity")+"']").attr('selected', 'selected');
          $("#edit_lead_time_max").val($(this).data("lead_time_max"));
          $("#edit_occupancy_percent").val($(this).data("occupancy_percent"));
          $("#edit_discount_rate").val($(this).data("discount_rate"));
          $("#edit_discount_rate").val($(this).data("discount_rate"));
          $("#edit_meal_inclusion").val($(this).data("meal_inclusion"));
          $("#edit_meal_description").val($(this).data("meal_description"));
          $("#edit_rate_plan_id").val($(this).data("id"));

            // $("#EditRoomType #room_type_id").val($(this).data("id"));
            // $("#EditRoomType #name").val($(this).data("name"));
            // $("#EditRoomType #base_price").val($(this).data("base_price"));
            // $("#EditRoomType #max_num_adult").val($(this).data("max_num_adult"));
            // $("#EditRoomType #max_num_children").val($(this).data("max_num_children"));
        });
    });
</script>
