<?php
// var_dump($roomTypes); die();
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Promos
            </header>

            <div class="panel-body">
            	<div class="col-md-3 mbot15">
                  <a href="#AddPromo" data-toggle="modal" class="btn btn-info">Add</a>
              </div>
              <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                  <?php echo $this->session->flashdata('alert_msg'); ?>
              </span>
            	<div class="table-responsive col-md-12">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Name</th>
            					<th>Description</th>
                      <th>Code</th>
                      <th>Validity</th>
                      <th width="15%">Rate Plan</th>
            					<th>Nights<br>(Min - Max)</th>
                      <th>Rooms<br>(Min - Max)</th>
                      <!-- <th>Max Lead Time</th> -->
                      <th>Discount</th>
                      <th>Actions</th>
            				</tr>
            			</thead>
            			<tbody>
            				<?php
                    if(count($promos) > 0) :
              				foreach ($promos as $promo) { ?>
              				<tr>
              					<td><?php echo $promo->name; ?></td>
  	            				<td><?php echo $promo->description; ?></td>
  	            				<td><?php echo $promo->code; ?></td>
  	            				<td><?php echo $promo->date_range; ?></td>
                        <td><?php echo $promo->rate_plan; ?></td>
                        <td><?php echo $promo->min_nights." - ".$promo->max_nights; ?></td>
                        <td><?php echo $promo->min_rooms." - ".$promo->max_rooms; ?></td>
                        <!-- <td><?php echo $promo->max_lead_time; ?></td> -->
                        <td><?php echo $promo->discount_percent."%"; ?></td>
                        <td>
                          <button class="btn btn-xs btn-info edit-promos" title="edit" href="#EditPromo"
                                  data-toggle="modal"
                                  data-name="<?php echo $promo->name; ?>"
                                  data-description="<?php echo $promo->description; ?>"
                                  data-code="<?php echo $promo->code; ?>"
                                  data-start_date="<?php echo date("m/d/Y", strtotime($promo->start_date)); ?>"
                                  data-end_date="<?php echo date("m/d/Y", strtotime($promo->end_date)); ?>"
                                  data-rate_plan_id="<?php echo str_replace("*","",$promo->rate_plan_id); ?>"
                                  data-discount_percent="<?php echo $promo->discount_percent; ?>"
                                  data-min_nights="<?php echo $promo->min_nights; ?>"
                                  data-max_nights="<?php echo $promo->max_nights; ?>"
                                  data-min_rooms="<?php echo $promo->min_rooms; ?>"
                                  data-max_rooms="<?php echo $promo->max_rooms; ?>"
                                  data-id="<?php echo $promo->promo_id; ?>"
                                  >
                                  <i class="fa fa-pencil"></i>
                          </button>
                          <button class='btn btn-xs btn-danger' title="Delete" onclick="deleteRoomType(<?php echo $promo->promo_id; ?>)"><i class='fa fa-times'></i></button>
                        </td>
  	            			</tr>
              				<?php
              				}
                    else: ?>
                    <tr>
                      <td colspan="10"><center>No items found. Add now by clicking the "Add" button.</center></td>
                    </tr>

                    <?php
                    endif;
              				?>
            			</tbody>
            		</table>
    			</div>
            </div>
        </section>
	</div>
</div>


		<!-- page end-->
	</section>
</section>
<!--main content end-->

<!-- Modals Start -->

	<!-- Add Promo Modal -->
    <div class="modal fade " id="AddPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/addPromo'); ?>">
            	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Promo</h4>
                </div>

                <div class="modal-body">

                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" class="form-control">
                	</div>

                	<div class="form-group">
                		<label>Description</label>
                		<textarea class="form-control" name="description"></textarea>
                	</div>

                  <div class="form-group">
                    <label>Code</label>
                    <input type="text" class="form-control" name="code">
                  </div>

                  <div class="form-group">
                    <label>Date Range</label>
                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                      <input type="text" class="form-control dpd1" name="start_date" placeholder="Start Audit Date">
                      <span class="input-group-addon">To</span>
                      <input type="text" class="form-control dpd2" name="end_date" placeholder="End Audit Date">
                  	</div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Rate Plan</label>
                        <select name="rate_plan_id[]" class="form-control" multiple>
                          <?php foreach($rate_plans as $rate) { ?>
                            <option value="<?php echo $rate->rate_plan_id; ?>"><?php echo $rate->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <label>Discount Percent</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="discount_percent" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <label class="col-sm-3 control-label">Check-in Nights (Min - Max)</label>
                      <div class="col-sm-4">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="min_nights" value="1">
                          <span class="input-group-addon">min days</span>
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="max_nights">
                          <span class="input-group-addon">max days</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <label class="col-sm-3 control-label"># of Rooms</label>
                      <div class="col-sm-4">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="min_rooms" value="1">
                          <span class="input-group-addon">min rooms</span>
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="max_rooms">
                          <span class="input-group-addon">max rooms</span>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" name="" value="Add" class="btn btn-success">
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Add Promo End  -->

    <!-- Edit Promo Modal -->
      <div class="modal fade " id="EditPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog">
              <div class="modal-content">
              	<form method="post" action="<?php echo base_url('admin/editPromo'); ?>">
              	<div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                      <h4 class="modal-title">Add Promo</h4>
                  </div>

                  <div class="modal-body">

                  	<div class="form-group">
                  		<label>Name</label>
                  		<input type="text" name="name" class="form-control" id="edit_name">
                  	</div>

                  	<div class="form-group">
                  		<label>Description</label>
                  		<textarea class="form-control" name="description" id="edit_description"></textarea>
                  	</div>

                    <div class="form-group">
                      <label>Code</label>
                      <input type="text" class="form-control" name="code" id="edit_code">
                    </div>

                    <div class="form-group">
                      <label>Date Range</label>
                      <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                        <input type="text" class="form-control dpd1" name="start_date" placeholder="Start Audit Date" id="edit_start_date">
                        <span class="input-group-addon">To</span>
                        <input type="text" class="form-control dpd2" name="end_date" placeholder="End Audit Date" id="edit_end_date">
                    	</div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <div class="col-sm-6">
                          <label>Rate Plan</label>
                          <select name="rate_plan_id[]" class="form-control" multiple id="edit_rate_plan_id">
                            <?php foreach($rate_plans as $rate) { ?>
                              <option value="<?php echo $rate->rate_plan_id; ?>"><?php echo $rate->name; ?></option>
                            <?php } ?>
                          </select>
                        </div>
                        <div class="col-sm-6">
                          <label>Discount Percent</label>
                          <div class="input-group m-bot15">
                            <input type="number" name="discount_percent" class="form-control" id="edit_discount_percent">
                            <span class="input-group-addon">%</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <label class="col-sm-3 control-label">Check-in Nights (Min - Max)</label>
                        <div class="col-sm-4">
                          <div class="input-group m-bot15">
                            <input type="number" class="form-control" name="min_nights" value="1" id="edit_min_nights">
                            <span class="input-group-addon">min days</span>
                          </div>
                        </div>
                        <div class="col-sm-5">
                          <div class="input-group m-bot15">
                            <input type="number" class="form-control" name="max_nights" id="edit_max_nights">
                            <span class="input-group-addon">max days</span>
                          </div>
                        </div>
                      </div>
                    </div>

                    <div class="form-group">
                      <div class="row">
                        <label class="col-sm-3 control-label"># of Rooms</label>
                        <div class="col-sm-4">
                          <div class="input-group m-bot15">
                            <input type="number" class="form-control" name="min_rooms" value="1" id="edit_min_rooms">
                            <span class="input-group-addon">min rooms</span>
                          </div>
                        </div>
                        <div class="col-sm-5">
                          <div class="input-group m-bot15">
                            <input type="number" class="form-control" name="max_rooms" id="edit_max_rooms">
                            <span class="input-group-addon">max rooms</span>
                          </div>
                        </div>
                      </div>
                    </div>

                  </div>
                  <div class="modal-footer">
                      <input type="hidden" name="promo_id" id="promo_id" value="">
                      <input type="submit" name="" value="Edit" class="btn btn-success">
                  </div>
                  </form>
              </div>
          </div>
      </div>
      <!-- Edit Promo End  -->

<!-- Modals END -->

<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";

    function deleteRoomType(id)
    {
        if (confirm("Are you sure you want to do this?")) {
            window.location = base_url + "/admin/deleteRoomType/" + id
        }
    }

	$(function(){
        $(".edit-promos").on("click", function(){
          // alert("hey");
          $("#edit_name").val($(this).data("name"));
          $("#edit_description").val($(this).data("description"));
          $("#edit_code").val($(this).data("code"));
          $("#edit_start_date").val($(this).data("start_date"));
          $("#edit_end_date").val($(this).data("end_date"));
          var rate_plan_ids = $(this).data("rate_plan_id").toString();

          if(rate_plan_ids.indexOf(",") >= 0) {
            $("#edit_rate_plan_id").val(rate_plan_ids.split(','));
          }
          else {
            $("#edit_rate_plan_id").val(rate_plan_ids);
          }

          $("#edit_discount_percent").val($(this).data("discount_percent"));
          $("#edit_min_nights").val($(this).data("min_nights"));
          var max_nights = $(this).data("max_nights");
          if(max_nights == "No Max")
          {
            max_nights = 0;
          }
          $("#edit_max_nights").val(max_nights);
          $("#edit_min_rooms").val($(this).data("min_rooms"));
          var max_rooms = $(this).data("max_rooms");
          if(max_rooms == "No Max")
          {
            max_rooms = 0;
          }
          $("#edit_max_rooms").val(max_rooms);
          $("#promo_id").val($(this).data("id"));
        });
    });
</script>
