<?php
setlocale(LC_MONETARY, 'en_PH');
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Reservations
            </header>

            <div class="panel-body">
            	<div class="table-responsive col-md-12">
                <?php
                $alert_msg = $this->session->flashdata('alert_msg');
                if($alert_msg != ""): ?>
                <div class="alert <?php echo $this->session->flashdata('alert_class'); ?> fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <?php echo $alert_msg; ?>
                </div>
                <?php endif; ?>

                <div class="alert alert-info fade in">
                    <button data-dismiss="alert" class="close close-sm" type="button">
                        <i class="fa fa-times"></i>
                    </button>
                    <strong>Heads up!</strong> All <strong>dates</strong> in this table are format MM/DD/YYYY.
                </div>

            		<table class="table table-bordered">
            			<thead>
                    <tr>
                      <th colspan="4"><center>Booking Details</center></th>
                      <th colspan="2"><center>Guest Details</center></th>
                      <th colspan="3"><center>Room Details</center></th>
                    </tr>
            				<tr>
            					<th>Date In</th>
                      <th>Date Out</th>
                      <th>Status</th>
                      <th>Price</th>
                      <!-- <th>Rate Plan</th> -->
                      <!-- <th>Promos</th> -->
                      <th>Name</th>
                      <th>Contact</th>
                      <!-- <th>Email</th> -->
                      <th>Actions</th>
                      <th>Room Type</th>
                      <th>Room #</th>
            				</tr>
            			</thead>
            			<tbody>
            				<?php
                    if(count($reservations) > 0) :
              				foreach ($reservations as $r) { ?>
              				<tr>
                        <td><?php echo $r->date_in; ?></td>
                        <td><?php echo $r->date_out; ?></td>
                        <td><?php echo $r->status; ?></td>
                        <td><?php echo money_format('%i', $r->price); ?></td>
                        <!-- <td><?php echo $r->rate_name; ?></td> -->
                        <!-- <td><?php echo $r->promo_name; ?></td> -->
                        <td><?php echo $r->guest_name; ?></td>
                        <td><?php echo $r->contact; ?></td>
                        <!-- <td><?php echo $r->email; ?></td> -->
                        <td>
                          <div class="btn-group">
                            <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle btn-sm" type="button" aria-expanded="false">
                              Manage <span class="caret"></span>
                            </button>
                            <ul role="menu" class="dropdown-menu">
                              <li><a href="#">Check in</a></li>
                              <li><a href="#">Check Out</a></li>
                              <li><a href="#">Assign to new room</a></li>
                              <li><a href="#">See More Details</a></li>
                              <li class="divider"></li>
                              <li><a href="#">Cancel Booking</a></li>
                              </ul>
                            </div>
                            <!-- <button class="btn btn-xs btn-info edit-promos" title="edit" href="#EditPromo"
                            data-toggle="modal"

                            >
                            <i class="fa fa-pencil"></i>
                          </button> -->
                          <!-- <button class='btn btn-xs btn-danger' title="Delete" onclick=""><i class='fa fa-times'></i></button> -->
                        </td>
                        <td><?php echo $r->name; ?></td>
                        <td><?php echo "None"; ?></td>
  	            			</tr>
              				<?php
              				}
                    else: ?>
                    <tr>
                      <td colspan="10"><center>No items found. Add now by clicking the "Add" button.</center></td>
                    </tr>

                    <?php
                    endif;
              				?>
            			</tbody>
            		</table>
    			</div>
            </div>
        </section>
	</div>
</div>


		<!-- page end-->
	</section>
</section>
<!--main content end-->

<!-- Modals Start -->

	<!-- Add Promo Modal -->
    <div class="modal fade " id="AddPromo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/addPromo'); ?>">
            	<div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Promo</h4>
                </div>

                <div class="modal-body">

                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" class="form-control">
                	</div>

                	<div class="form-group">
                		<label>Description</label>
                		<textarea class="form-control" name="description"></textarea>
                	</div>

                  <div class="form-group">
                    <label>Code</label>
                    <input type="text" class="form-control" name="code">
                  </div>

                  <div class="form-group">
                    <label>Date Range</label>
                    <div class="input-group input-large" data-date="13/07/2013" data-date-format="mm/dd/yyyy">
                      <input type="text" class="form-control dpd1" name="start_date" placeholder="Start Audit Date">
                      <span class="input-group-addon">To</span>
                      <input type="text" class="form-control dpd2" name="end_date" placeholder="End Audit Date">
                  	</div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-sm-6">
                        <label>Rate Plan</label>
                        <select name="rate_plan_id[]" class="form-control" multiple>
                          <?php foreach($rate_plans as $rate) { ?>
                            <option value="<?php echo $rate->rate_plan_id; ?>"><?php echo $rate->name; ?></option>
                          <?php } ?>
                        </select>
                      </div>
                      <div class="col-sm-6">
                        <label>Discount Percent</label>
                        <div class="input-group m-bot15">
                          <input type="number" name="discount_percent" class="form-control">
                          <span class="input-group-addon">%</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <label class="col-sm-3 control-label">Check-in Nights (Min - Max)</label>
                      <div class="col-sm-4">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="min_nights" value="1">
                          <span class="input-group-addon">min days</span>
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="max_nights">
                          <span class="input-group-addon">max days</span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <label class="col-sm-3 control-label"># of Rooms</label>
                      <div class="col-sm-4">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="min_rooms" value="1">
                          <span class="input-group-addon">min rooms</span>
                        </div>
                      </div>
                      <div class="col-sm-5">
                        <div class="input-group m-bot15">
                          <input type="number" class="form-control" name="max_rooms">
                          <span class="input-group-addon">max rooms</span>
                        </div>
                      </div>
                    </div>
                  </div>

                </div>
                <div class="modal-footer">
                    <input type="submit" name="" value="Add" class="btn btn-success">
                </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Add Promo End  -->

<!-- Modals END -->

<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";

    function deleteRoomType(id)
    {
        if (confirm("Are you sure you want to do this?")) {
            window.location = base_url + "/admin/deleteRoomType/" + id
        }
    }

	$(function(){
        $(".edit-promos").on("click", function(){
          // alert("hey");
          $("#edit_name").val($(this).data("name"));
          $("#edit_description").val($(this).data("description"));
          $("#edit_code").val($(this).data("code"));
          $("#edit_start_date").val($(this).data("start_date"));
          $("#edit_end_date").val($(this).data("end_date"));
          var rate_plan_ids = $(this).data("rate_plan_id").toString();

          if(rate_plan_ids.indexOf(",") >= 0) {
            $("#edit_rate_plan_id").val(rate_plan_ids.split(','));
          }
          else {
            $("#edit_rate_plan_id").val(rate_plan_ids);
          }

          $("#edit_discount_percent").val($(this).data("discount_percent"));
          $("#edit_min_nights").val($(this).data("min_nights"));
          var max_nights = $(this).data("max_nights");
          if(max_nights == "No Max")
          {
            max_nights = 0;
          }
          $("#edit_max_nights").val(max_nights);
          $("#edit_min_rooms").val($(this).data("min_rooms"));
          var max_rooms = $(this).data("max_rooms");
          if(max_rooms == "No Max")
          {
            max_rooms = 0;
          }
          $("#edit_max_rooms").val(max_rooms);
          $("#promo_id").val($(this).data("id"));
        });
    });
</script>
