<?php
// var_dump($roomTypes); die();
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Room Types
                <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                    <?php echo $this->session->flashdata('alert_msg'); ?>
                </span>
            </header>

            <div class="panel-body">
            	<div class="col-md-3 mbot15">
                    <a href="#AddRoomType" data-toggle="modal" class="btn btn-info">Add</a>
                </div>

            	<div class="table-responsive col-md-12">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Name</th>
            					<th>Base Price</th>
            					<th>Max No. of Adults</th>
            					<th>Max No. of Children</th>
            					<th>Inclusions</th>

            					<?php if (@$dev): ?>
            					<th>created_at</th>
            					<th>updated_at</th>
            					<?php endif ?>

                                <th>Actions</th>

            				</tr>
            			</thead>
            			<tbody>
            				<?php
            				foreach ($roomTypes as $key => $value) {
            					$inclusion_arr_string = "";
            				?>
            				<tr>
            					<td><?php echo $value->name; ?></td>
	            				<td><?php echo $value->base_price; ?></td>
	            				<td><?php echo $value->max_num_adult; ?></td>
	            				<td><?php echo $value->max_num_children; ?></td>
	            				<td>
	            					<?php
	            					foreach ($value->inclusions as $key => $inclusion) {
	            						$inclusion_arr[] = $inclusion->title;
	            					}
	            					if (@$inclusion_arr) {
	            						$inclusion_arr_string = implode(", ", $inclusion_arr);
	            						echo $inclusion_arr_string;
	            					}
	            					?>
	            				</td>

	            				<?php if (@$dev): ?>
	            				<td><?php echo $value->created_at; ?></td>
	            				<td><?php echo $value->updated_at; ?></td>
	            				<?php endif ?>

                                <td>
                                    <!-- <button href="#EditRoomType" data-toggle="modal" class='btn btn-xs edit-room-type' title='Edit'
                                    	data-id="<?php echo $value->room_type_id; ?>"
                                    	data-name="<?php echo $value->name; ?>"
                                    	data-base_price="<?php echo $value->base_price; ?>"
                                    	data-max_num_adult="<?php echo $value->max_num_adult; ?>"
                                    	data-max_num_children="<?php echo $value->max_num_children; ?>"
                                    	data-inclusions="<?php echo $inclusion_arr_string; ?>">
                                        <i class='fa fa-pencil'></i>
                                    </button> -->
                                    <a href="<?php echo base_url('admin/roomType/') . $value->room_type_id; ?>"><i class='fa fa-pencil'></i></a
                                    <button class='btn btn-xs btn-danger' title="Delete" onclick="deleteRoomType(<?php echo $value->room_type_id; ?>)"><i class='fa fa-times'></i></button>
                                </td>
	            			</tr>
            				<?php
            				}
            				?>
            			</tbody>
            		</table>
    			</div>
            </div>
        </section>
	</div>
</div>


		<!-- page end-->
	</section>
</section>
<!--main content end-->

<!-- Modals Start -->

	<!-- Add Room Type Modal -->
    <div class="modal fade " id="AddRoomType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/addRoomType'); ?>">
            	<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Room Type</h4>
                </div>

                <div class="modal-body">
                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Base Price</label>
                		<input type="number" name="base_price" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Max No. of Adults</label>
                		<input type="number" name="max_num_adult" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Max No. of Children</label>
                		<input type="number" name="max_num_children" class="form-control">
                	</div>
                	<div class="row form-group">
                		<label class="col-sm-2 control-label col-lg-2">Inclusions</label>
                		<div class="col-lg-10">
                			<?php
                			foreach ($inclusions as $key => $value) {
                			?>

                			<div class="checkbox">
                                <label>
                                    <input type="checkbox" value="<?php echo $value->inclusion_id; ?>" name="inclusions[]">
                                    <?php echo $value->title; ?>
                                </label>
                            </div>

                			<?php
                			}
                			?>
                		</div>
                	</div>

                </div>

                <div class="modal-footer">
                    <input type="submit" name="" value="Add" class="btn btn-success btn-xs">
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Room Type Modal -->
    <div class="modal fade " id="EditRoomType" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/editRoomType'); ?>">
            	<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Room Type</h4>
                </div>

                <div class="modal-body">
                	<div class="form-group">
                		<label>Name</label>
                		<input type="text" name="name" id="name" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Base Price</label>
                		<input type="number" name="base_price" id="base_price" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Max No. of Adults</label>
                		<input type="number" name="max_num_adult" id="max_num_adult" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Max No. of Children</label>
                		<input type="number" name="max_num_children" id="max_num_children" class="form-control">
                	</div>
                	<div class="row form-group">
                		<label class="col-sm-2 control-label col-lg-2">Inclusions</label>
                		<div class="col-lg-10">
                			<?php
                			foreach ($inclusions as $key => $value) {
                			?>

                			<div class="checkbox">
                                <label>
                                    <input type="checkbox" value="<?php echo $value->inclusion_id; ?>" name="inclusions[]">
                                    <?php echo $value->title; ?>
                                </label>
                            </div>

                			<?php
                			}
                			?>
                		</div>
                	</div>
                </div>

                <div class="modal-footer">
           			<input type="hidden" name="room_type_id" id="room_type_id">
                    <input type="submit" name="" value="Edit" class="btn btn-success btn-xs">
                </div>
                </form>
            </div>
        </div>
    </div>

<!-- Modals END -->

<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";
    function deleteRoomType(id)
    {
        if (confirm("Are you sure you want to do this?")) {
            window.location = base_url + "/admin/deleteRoomType/" + id
        }
    }

	$(function(){
        $(".edit-room-type").on("click", function(){
            $("#EditRoomType #room_type_id").val($(this).data("id"));
            $("#EditRoomType #name").val($(this).data("name"));
            $("#EditRoomType #base_price").val($(this).data("base_price"));
            $("#EditRoomType #max_num_adult").val($(this).data("max_num_adult"));
            $("#EditRoomType #max_num_children").val($(this).data("max_num_children"));
        });
    });
</script>
