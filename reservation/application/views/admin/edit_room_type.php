<?php
// var_dump($roomType); die();
$selected_inclusion_ids = array();
foreach ($selected_inclusions as $value) {
	$selected_inclusion_ids[] = $value->inclusion_id;
}
// var_dump($selected_inclusion_ids); die();
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

		<div class="row">
			<div class="col-lg-12">
				<!--breadcrumbs start -->
				<ul class="breadcrumb">
					<li><a href="<?php echo base_url('admin/roomTypes') ?>"> &laquo; Back</a></li>
					<li class="active">Edit Room Type</li>
				</ul>
				<!--breadcrumbs end -->
				<section class="panel">
					<header class="panel-heading">
						<?php //echo uri_string(); ?>
					</header>
					<div class="panel-body">
						<form role="form" method="POST" action="<?php echo base_url("admin/updateRoomType"); ?>">

							<div class="form-group">
								<label>Name</label>
								<input type="text" class="form-control" placeholder="Name" name="name" value="<?php echo $roomType[0]->name; ?>">
							</div>

							<div class="form-group">
								<label>Base Price</label>
								<input type="text" class="form-control" placeholder="Base Price" name="base_price" value="<?php echo $roomType[0]->base_price; ?>">
							</div>

							<div class="form-group">
								<label>Max No. of Adults</label>
								<input type="number" class="form-control" placeholder="" name="max_num_adult" value="<?php echo $roomType[0]->max_num_adult; ?>">
							</div>

							<div class="form-group">
								<label>Max No. of Children</label>
								<input type="number" class="form-control" placeholder="" name="max_num_children" value="<?php echo $roomType[0]->max_num_children; ?>">
							</div>

							<div class="row form-group">
								<label class="col-sm-2 control-label col-lg-2">Inclusions</label>
								<div class="col-lg-10">
									<?php
									foreach ($inclusions as $key => $value) {
										?>

										<div class="checkbox">
											<label>
												<input type="checkbox" value="<?php echo $value->inclusion_id; ?>" name="inclusions[]" <?php echo (in_array($value->inclusion_id, $selected_inclusion_ids)) ? 'checked' : ''; ?>>
												<?php echo $value->title; ?>
											</label>
										</div>

										<?php
									}
									?>
								</div>
							</div>
							<input type="hidden" name="room_type_id" value="<?php echo $roomType[0]->room_type_id; ?>">
							<button type="Submit" class="btn btn-success btn-block">Save</button>
						</form>
					</div>
				</section>
			</div>
		</div>

		<!-- page end-->
	</section>
</section>
<!--main content end-->
