<?php
// var_dump($mails); die();
?>
<!--main content start-->
<section id="main-content">
  <section class="wrapper site-min-height">
    <!-- page start-->

    <div class="row">
      <div class="col-lg-12">
        <section class="panel">
          <header class="panel-heading">Emails</header>

          <div class="panel-body">

            <div class="table-responsive col-md-12">
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Contact No.</th>
                    <th>Facility</th>
                    <th>Message</th>
                    <th>Date Sent</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  foreach ($mails as $key => $value) {
                    ?>
                    <tr>
                      <td><?php echo $value->title . " " . $value->name; ?></td>
                      <td><?php echo $value->email; ?></td>
                      <td><?php echo $value->contact; ?></td>
                      <td><?php echo $value->facility; ?></td>
                      <td><?php echo $value->message; ?></td>
                      <td><?php echo $value->date_sent; ?></td>
                    </tr>
                    <?php
                  }
                  ?>
                </tbody>
              </table>
            </div>
          </div>
        </section>
      </div>
    </div>

    <!-- page end-->
  </section>
</section>
<!--main content end-->
