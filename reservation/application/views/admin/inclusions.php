<?php
// var_dump($inclusions); die();
if (@$dev) {
	// var_dump($dev); die();
}
?>
<!--main content start-->
<section id="main-content">
	<section class="wrapper site-min-height">
		<!-- page start-->

<div class="row">
	<div class="col-lg-12">
		<section class="panel">
			<header class="panel-heading">
                Inclusion List 
                <span style="font-size: 14px; color: <?php echo $this->session->flashdata('alert_color'); ?>">
                    <?php echo $this->session->flashdata('alert_msg'); ?>
                </span>
            </header>

            <div class="panel-body">
            	<div class="col-md-3 mbot15">
                    <a href="#AddInclusion" data-toggle="modal" class="btn btn-info">Add</a>
                </div>

            	<div class="table-responsive col-md-12">
            		<table class="table table-bordered">
            			<thead>
            				<tr>
            					<th>Title</th>
            					<th>Description</th>

            					<?php if (@$dev): ?>
            					<th>created_at</th>
            					<th>updated_at</th>
            					<?php endif ?>

                                <th>Actions</th>

            				</tr>
            			</thead>
            			<tbody>
            				<?php
            				foreach ($inclusions as $key => $value) {
            				?>
            				<tr>
            					<td><?php echo $value->title; ?></td>
	            				<td><?php echo $value->description; ?></td>

	            				<?php if (@$dev): ?>
	            				<td><?php echo $value->created_at; ?></td>
	            				<td><?php echo $value->updated_at; ?></td>
	            				<?php endif ?>

                                <td>
                                    <button href="#EditInclusion" data-toggle="modal" class='btn btn-xs edit-inclusion' title='Edit'
                                        data-id="<?php echo $value->inclusion_id; ?>"
                                        data-title="<?php echo $value->title; ?>"
                                        data-description="<?php echo $value->description; ?>">
                                        <i class='fa fa-pencil'></i>
                                    </button>
                                    <button class='btn btn-xs btn-danger' title="Delete" onclick="deleteInclusion(<?php echo $value->inclusion_id; ?>)"><i class='fa fa-times'></i></button>
                                </td>
	            			</tr>
            				<?php
            				}
            				?>
            			</tbody>
            		</table>
    			</div>
            </div>
        </section>
	</div>
</div>

<!-- Modals Start -->
	
	<!-- Add Inclusion Modal -->
    <div class="modal fade " id="AddInclusion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
            	<form method="post" action="<?php echo base_url('admin/addInclusion'); ?>">
            	<div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Add Inclusion</h4>
                </div>

                <div class="modal-body">
                	<div class="form-group">
                		<label>Title</label>
                		<input type="text" name="title" class="form-control">
                	</div>
                	<div class="form-group">
                		<label>Description</label>
                		<textarea class="form-control" name="description"></textarea>
                	</div>
                </div>

                <div class="modal-footer">
                    <!-- <a href="javascript:void(0);" class="btn btn-success btn-xs add-room-btn">Add</a> -->

                    <input type="submit" name="" value="Add" class="btn btn-success btn-xs">
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Edit Inclusion Modal -->
    <div class="modal fade " id="EditInclusion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <form method="post" action="<?php echo base_url('admin/editInclusion'); ?>">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Edit Inclusion</h4>
                </div>

                <div class="modal-body">
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" id="title">
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <textarea class="form-control" name="description" id="description"></textarea>
                    </div>
                </div>

                <div class="modal-footer">
                    <!-- <a href="javascript:void(0);" class="btn btn-success btn-xs add-room-btn">Add</a> -->
                    <input type="hidden" name="inclusion_id" id="inclusion_id">
                    <input type="submit" name="" value="Submit" class="btn btn-success btn-xs">
                </div>
                </form>
            </div>
        </div>
    </div>


<!-- Modals End -->

		<!-- page end-->
	</section>
</section>
<!--main content end-->

<script type="text/javascript">
    var base_url = "<?php echo base_url(); ?>";
        
    function deleteInclusion(id)
    {
        if (confirm("Are you sure you want to do this?")) {
            window.location = base_url + "/admin/deleteInclusion/" + id
        }
    }

    $(function(){
        $(".edit-inclusion").on("click", function(){
            $("#EditInclusion #inclusion_id").val($(this).data("id"));
            $("#EditInclusion #title").val($(this).data("title"));
            $("#EditInclusion #description").val($(this).data("description"));
        });
    });
</script>