<?php
/* Template Name: Poro Point Casino Events */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.casino__elements .row .banner-events {
  height: 376px;
}
.casino__elements .row .banner-events img {
  width: 99%;
  height: 99%;
  object-fit: cover;
}
</style>

<div class="first-half">
  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image', 35); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

  </div>

  <div class="casino">
      <div class="container">
        <div class="title">
            <h2>Thunderbird Resorts and Casinos Poro Point - <?php echo get_field('header', 35); ?></h2>
            <?php echo apply_filters('the_content', get_post_field('post_content', 35)); ?>
        </div>

        <!-- Casino Events START -->
        <div class="casino__elements">
            <div class="heading">
                <h3><?php echo get_field('events_header'); ?></h3>
                <p><?php echo get_field('events_sub_header'); ?></p>
            </div>
            <div class="row">
                <?php
                foreach (get_field('events_images') as $value) {
                ?>

                <div class="banner banner-events col-lg-6 col-md-6 col-sm-6 col-xs-12 popup-gallery">
                    <a href="<?php echo $value['images'] ?>">
                      <img src="<?php echo $value['images']; ?>" alt="">
                    </a>
                </div>

                <?php
                }
                ?>
            </div>
        </div>
        <!-- Casino Events END -->

      </div>
  </div>
</div>

<?php
endwhile;
get_footer("poro");
?>
