<?php
/* Template Name: Poro Point Casino Gaming Facilities */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.casino__elements .row .banner-facilities {
  width: 390px;
  height: 256px;
}
.casino__elements .row .banner-facilities img {
  width: 99%;
  height: 99%;
  object-fit: cover;
}
</style>

<div class="first-half">
  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image', 35); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

  </div>

  <div class="casino">
      <div class="container">
        <div class="title">
            <h2><?php echo get_field('header', 35); ?></h2>
            <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
        </div>

        <!-- Casino Gaming Facilities START -->
        <div class="casino__elements custom">
            <div class="heading">
                <h3><?php echo get_field('facilities_header'); ?></h3>
                <p><?php echo get_field('facilities_sub_header'); ?></p>
            </div>
            <div class="row">
                <?php
                foreach (get_field('facilities_images') as $value) {
                ?>

                <div class="banner banner-facilities col-lg-4 col-md-4 col-sm-4 col-xs-12 popup-gallery">
                    <a href="<?php echo $value['images'] ?>">
                      <img src="<?php echo $value['images']; ?>" alt="">
                    </a>
                </div>

                <?php
                }
                ?>
            </div>
        </div>
        <!-- Casino Gaming Facilities END -->

      </div>
  </div>
</div>

<?php
endwhile;
get_footer("poro");
?>
