<?php
/* Template Name: Poro Point Facilities */
get_header("poro");
while(have_posts()): the_post();
?>

<style type="text/css">
.facilities-slider {
	height: 467px;
}
.facilities-slider .swiper-wrapper .swiper-slide {
	width: 456px;
}
.facilities-slider .swiper-wrapper .swiper-slide img {
	height: 99%;
	width: 99%;
	object-fit: cover;
}
</style>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

	<div class="facilities">
		<div class="container">
			<div class="title">
				<h2><?php echo get_field('header') ?></h2>
				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
			</div>
		</div>
		<?php
		$ctr = 0;
		foreach (get_field('facilities') as $value) {
			if ($ctr % 2 == 0) {
		?>

		<div class="promo custom right item<?php echo $ctr; ?>">
			<div class="container">
				<div class="row">
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3><?php echo $value['main_title']; ?></h3>
						<p>
							<?php
							foreach ($value['items'] as $item) {
								echo $item['title'] . "<br>";
							}
							?>
						</p>

					</div>
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container facilities-slider promo-slider<?php echo $ctr; ?>">
							<div class="swiper-wrapper">

								<?php
								foreach ($value['items'] as $item) {
								?>

								<div class="swiper-slide">
									<a href="<?php echo $item['image']; ?>" class="image-popup">
										<img src="<?php echo $item['image']; ?>" alt="">

										<div class="meta blue">
											<span><?php echo $item['title']; ?></span><br>
											<!-- <a href="#">LEARN MORE</a> -->
										</div>
									</a>
								</div>

								<?php
								}
								?>

								<div class="swiper-slide">
                    <img src="" alt="">
                </div>

							</div>
						</div>
					</div>

				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<?php
			}
			else {
		?>

		<div class="promo custom left item<?php echo $ctr; ?>">
			<div class="container">
				<div class="row">
					<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
						<div class="swiper-container facilities-slider promo-slider<?php echo $ctr; ?>">
							<div class="swiper-wrapper">
								<?php
								foreach ($value['items'] as $item) {
								?>

								<div class="swiper-slide">
									<a href="<?php echo $item['image']; ?>" class="image-popup">
										<img src="<?php echo $item['image']; ?>" alt="">

										<div class="meta blue">
											<span><?php echo $item['title']; ?></span><br>
											<!-- <a href="#">LEARN MORE</a> -->
										</div>
									</a>
								</div>

								<?php
								}
								?>

								<div class="swiper-slide">
                    <img src="" alt="">
                </div>

							</div>
						</div>
					</div>
					<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
						<h3><?php echo $value['main_title']; ?></h3>
						<p>
							<?php
							foreach ($value['items'] as $item) {
								echo $item['title'] . "<br>";
							}
							?>
						</p>
					</div>
				</div>
				<div class="swiper-scrollbar"></div>
			</div>
		</div>

		<?php
			}
			$ctr++;
		}
		?>

	</div>
</div>

<?php
endwhile;
get_footer("poro");
?>
