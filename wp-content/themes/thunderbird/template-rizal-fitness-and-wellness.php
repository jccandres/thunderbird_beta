<?php
/* Template Name: Rizal Fitness and Wellness */
get_header("rizal");
while(have_posts()): the_post();
?>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

	</div>

	<div class="restaurant">
		<div class="container">
			<div class="title">
				<h2>Thunderbird Resorts and Casinos Poro Point - <?php echo get_field('header') ?></h2>
				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
			</div>

			<div class="list">
			  <?php
			  $ctr = 1;
			  foreach (get_field('facilities') as $value) {
			  ?>

			  <div class="item rest-item-<?php echo $ctr; ?>">
			    <div class="desc">
			      <h3><?php echo $value['main_title']; ?></h3>
			      <!-- <p>Content</p> -->
			    </div>
			    <div class="photos">
			      <div class="swiper-container rest rest-<?php echo $ctr; ?>">
			        <div class="swiper-wrapper">
			          <?php
			          foreach ($value['items'] as $item) {
			          ?>

			          <div class="swiper-slide">
			            <a href="<?php echo $item['image']; ?>" class="image-popup" title="<p><h3><?php echo $item['title']; ?></h3> <?php echo $item['caption']; ?></p>">
										<figure>
											<img src="<?php echo $item['image']; ?>" alt="">
										</figure>
									</a>
			          </div>

			          <?php
			          }
			          ?>
			        </div>
			      </div>
			      <div class="swiper-button-next"></div>
			      <div class="swiper-button-prev"></div>
			    </div>
			  </div>

			  <?php
				$ctr++;
			  }
			  ?>
			</div>

		</div>
	</div>
	
</div>

<?php
endwhile;
get_footer("rizal");
?>
