<?php
/* Template Name: Contact Us */
get_header("main");
while(have_posts()): the_post();
if (isset($_POST['submit_inquiry'])) {
	var_dump($_POST); die();
}
?>
<style>
.description .text h2, .description .text p {
	text-align: center;
}
.description .text {
	padding: 0 0 0;
	float: none;
}
</style>
<div class="welcome contactheader">
	<div class="container">
    <?php get_template_part( 'template-parts/navigation/front-page/nav', 'menu' ); ?>
  </div>
</div>
<div class="pagewrapper3">

	<div class="description">
		<div class="text">
				<h2><?php echo get_the_title(); ?></h2>
				<p><?php echo apply_filters('the_content', get_post_field('post_content')); ?></p>
		</div>
	</div>


	<!-- Contact Form -->
	<form method="POST" action="<?php echo get_home_url(); ?>/reservation/mail/send">
	  <div class="contactform">

			<?php if (isset($_GET['sent'])): ?>
			<ul>
				<li class="col1">
					<?php if ($_GET['sent'] == 1): ?>
						<label style="color: #27ae60;">Your inquiry has been sent. Thank you.</label>
					<?php elseif ($_GET['sent'] == 0) : ?>
						<label style="color: #c0392b;">Failed to send your inquiry. Please try again later.</label>
					<?php endif; ?>
				</li>
			</ul>
			<?php endif; ?>

			<ul>

	      <li class="col2">
	        <label>Title</label>
	        <span>
	          <select name="title">
	            <option value="Mr.">Mr.</option>
	            <option value="Ms.">Ms.</option>
	            <option value="Mrs.">Mrs.</option>
	          </select>
	        </span>
	      </li>

	      <li class="col2">
	        <label>Name</label>
	        <span>
	          <input type="text" name="name" required>
	        </span>
	      </li>

	      <li class="col2">
	        <label>Email Address</label>
	        <span>
	          <input type="email" name="email" required>
	        </span>
	      </li>

	      <li class="col2">
	        <label>Contact No.</label>
	        <span>
	          <input type="text" name="contact" required>
	        </span>
	      </li>

	      <li class="col2">
	        <label>Facility</label>
	        <span>
	          <select name="facility">
	            <option value="Hotel">Hotel</option>
	            <option value="Dining">Dining</option>
	            <option value="Careers">Careers</option>
	          </select>
	        </span>
	      </li>

	      <li class="col1">
	        <label>Message</label>
	        <span>
	          <textarea name="message" required></textarea>
	        </span>
	      </li>

	      <li class="col1">
	        <span>
	          <input type="submit">
	        </span>
	      </li>

	    </ul>
	  </div>
	</form>
  <!-- Contact Form -->

  <!-- Poro contact info -->
  <div class="contact-poro">
		<?php echo get_field('poro_info'); ?>
  </div>
  <!-- Poro contact info -->

  <!-- Rizal contact info -->
  <div class="contact-rizal">
		<?php echo get_field('rizal_info'); ?>
  </div>
  <!-- Rizal contact info -->



</div>

<?php
endwhile;
get_footer("main");
?>
