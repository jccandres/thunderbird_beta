<?php
/* Template Name: Poro Point Golf Club */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.casino__elements .row .banner-events {
  height: 376px;
}
.casino__elements .row .banner-events img {
  width: 99%;
  height: 99%;
  object-fit: cover;
}
</style>

<div class="first-half">
  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image'); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

  </div>

  <div class="casino rooms">
      <div class="container">
        <div class="title">
            <h2>Thunderbird Resorts and Casinos Poro Point - <?php echo get_field('header'); ?></h2>
            <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
        </div>
        <!-- <div class="benefits">
            <?php
            //foreach (get_field('golf_club_content') as $golf_club) {
            ?>

            <div class="item">
                <?php //echo $golf_club['content']; ?>
            </div>

            <?php
            //}
            ?>
        </div> -->

        <!-- Casino Events START -->
        <div class="casino__elements">
            <div class="row">
                <?php
                foreach (get_field('golf_club_images') as $value) {
                ?>

                <div class="banner banner-events col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <a href="<?php echo $value['images'] ?>" class="image-popup">
                      <img src="<?php echo $value['images']; ?>" alt="">
                    </a>
                </div>

                <?php
                }
                ?>
            </div>
        </div>
        <!-- Casino Events END -->

      </div>
  </div>
</div>

<?php
endwhile;
get_footer("poro");
?>
