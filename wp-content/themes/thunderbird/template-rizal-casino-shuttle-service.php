<?php
/* Template Name: Rizal Casino Shuttle Service */
get_header("rizal");
while(have_posts()): the_post();
?>

<div class="first-half">
  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image'); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

  </div>

  <div class="casino">

    <div class="container">
      <!--<div class="title">
          <h2><?php echo get_field('header'); ?></h2>
          <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
      </div>-->
      <ul class="flex-container">
        <?php
        $args = array('post_type' => 'shuttle_service');
        $the_query = new WP_Query($args);
        if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

          <li class="flex-item">
            <h4><?php echo get_the_title(); ?></h4>
            <ul class="timeloc">
              <li>
                <span><?php echo get_field('first_location') ?></span><span><?php echo get_field('second_location'); ?></span>
              </li>
              <?php
              $time1 = get_field('first_location_time');
              $time2 = get_field('second_location_time');
              $ctr = (count($time1) > count($time2)) ? count($time1) : count($time2);
              for ($i=0; $i < $ctr; $i++) {
              ?>

              <li class="<?php echo ($time1[$i]['marker'] != "") ? 'special' : ''; ?>">
                <?php echo ($time1[$i]) ? "<span>" . $time1[$i]['time'] . "</span>" : ""; ?>
                <?php echo ($time2[$i]) ? "<span>" . $time2[$i]['time'] . "</span>" : ""; ?>
              </li>

              <?php
              }
              ?>

            </ul>

            <?php if (get_field('note')): ?>
              <label class="<?php echo (get_field('style') == 'Style 2') ? 'special' : 'note'; ?>">
                <?php echo (get_field('style') == 'Style 1') ? '*' : ''; ?>
                <?php echo get_field('note'); ?>
              </label>
            <?php endif; ?>

          </li>

        <?php endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>

      </ul>

    </div>

  </div>

</div>


<?php
endwhile;
get_footer("rizal");
?>
