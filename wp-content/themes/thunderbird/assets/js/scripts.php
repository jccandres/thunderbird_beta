<script type="text/javascript">
    jQuery(document).ready(function() {
        new Swiper(".top-slider", {
            slidesPerView:1, loop:!0, navigation: {
                nextEl: ".welcome .swiper-button-next", prevEl: ".welcome .swiper-button-prev"
            }
            , pagination: {
                el: ".top-slider .swiper-pagination", clickable: !0
            }
        }
        ), new Swiper(".gallery-first", {
            slidesPerView:3, centeredSlides:true, loop:!0, spaceBetween:0, navigation: {
                nextEl: ".gallery .inner.first .swiper-button-next", prevEl: ".gallery .inner.first .swiper-button-prev"
            }
            , breakpoints: {
                1200: {
                    slidesPerView: 1
                }
            }
        }
        ), new Swiper(".gallery-second", {
            slidesPerView:2, loop:!0, spaceBetween:0, navigation: {
                nextEl: ".gallery .inner.second .swiper-button-next", prevEl: ".gallery .inner.second .swiper-button-prev"
            }
            , breakpoints: {
                1200: {
                    slidesPerView: 1
                }
            }
        }
        ), new Swiper(".events-slider", {
            slidesPerView:2, spaceBetween:0, scrollbar: {
                el: ".promo.casino-page .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
            }
            , breakpoints: {
                767: {
                    slidesPerView: 1
                }
            }
        }

        /* slider for facilities page START */
        <?php
        $ctr = 0;
        foreach (get_field('facilities') as $value) {
          if ($ctr % 2 == 0) {
        ?>

        ), new Swiper(".promo-slider<?php echo $ctr; ?>", {
            slidesPerView:3, spaceBetween:0, scrollbar: {
                el: ".promo.item<?php echo $ctr; ?> .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
            }
            , breakpoints: {
                767: {
                    slidesPerView: 1
                }
            }
        }

        <?php
          }
          else {
        ?>

        ), new Swiper(".promo-slider<?php echo $ctr; ?>", {
            slidesPerView:3, spaceBetween:0, initialSlide:6, scrollbar: {
                el: ".promo.item<?php echo $ctr; ?> .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
            }
            , breakpoints: {
                767: {
                    slidesPerView: 1
                }
            }
        }

        <?php
          }
          $ctr++;
        }
        ?>
        /* slider for facilities page END */

        /* default slider START */
        ), new Swiper(".promo-slider", {
            slidesPerView:3, spaceBetween:0, scrollbar: {
                el: ".promo .swiper-scrollbar", draggable: !0, dragSize: "51", hide: !1
            }
            , breakpoints: {
                767: {
                    slidesPerView: 1
                }
            }
        }
        /* default slider END */

        /* slider for fitness and wellness START */
        <?php
        if (is_page(710)) {
          $ctr = 1;
          foreach (get_field('facilities', 710) as $value) {
        ?>

          ), new Swiper(".rest-<?php echo $ctr; ?>", {
              slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
                  nextEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-next", prevEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-prev"
              }
              , breakpoints: {
                  992: {
                      slidesPerView: 2
                  }
                  , 767: {
                      slidesPerView: 1
                  }
              }
          }

        <?php
          }
          $ctr++;
        }
        ?>

        <?php
        if (is_page(712)) {
          $ctr = 1;
          foreach (get_field('facilities', 712) as $value) {
        ?>

          ), new Swiper(".rest-<?php echo $ctr; ?>", {
              slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
                  nextEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-next", prevEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-prev"
              }
              , breakpoints: {
                  992: {
                      slidesPerView: 2
                  }
                  , 767: {
                      slidesPerView: 1
                  }
              }
          }

        <?php
          }
          $ctr++;
        }
        ?>


        /* slider for fitness and wellness END */

        /* slider for poro restaurant page START */
        <?php
        if (is_page(24)) {
          $args = array('post_type' => 'poro_restaurant');
  				$the_query = new WP_Query($args);
  				$ctr = 1;
  				if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

          ), new Swiper(".rest-<?php echo $ctr; ?>", {
              slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
                  nextEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-next", prevEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-prev"
              }
              , breakpoints: {
                  992: {
                      slidesPerView: 2
                  }
                  , 767: {
                      slidesPerView: 1
                  }
              }
          }

          <?php $ctr++; endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>
        <?php
        }
        ?>

        <?php
        if (is_page(39)) {
          $args = array('post_type' => 'rizal_restaurant');
  				$the_query = new WP_Query($args);
  				$ctr = 1;
  				if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

          ), new Swiper(".rest-<?php echo $ctr; ?>", {
              slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
                  nextEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-next", prevEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-prev"
              }
              , breakpoints: {
                  992: {
                      slidesPerView: 2
                  }
                  , 767: {
                      slidesPerView: 1
                  }
              }
          }

          <?php $ctr++; endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>
        <?php
        }
        ?>
        /* slider for poro restaurant page END */

        /* slider for occasions page START */
        <?php
        if (is_page(653) || is_page(655)) {
          $ctr = 1;
          foreach (get_field('occassions') as $value) {
        ?>

        ), new Swiper(".rest-<?php echo $ctr; ?>", {
            slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
                nextEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-next", prevEl: ".restaurant .item.rest-item-<?php echo $ctr; ?> .swiper-button-prev"
            }
            , breakpoints: {
                992: {
                    slidesPerView: 2
                }
                , 767: {
                    slidesPerView: 1
                }
            }
        }

        <?php
          $ctr++;
          }
        }
        ?>
        /* slider for occasions page END */

        // ), new Swiper(".rest-first", {
        //     slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
        //         nextEl: ".restaurant .item.first .swiper-button-next", prevEl: ".restaurant .item.first .swiper-button-prev"
        //     }
        //     , breakpoints: {
        //         992: {
        //             slidesPerView: 2
        //         }
        //         , 767: {
        //             slidesPerView: 1
        //         }
        //     }
        // }
        // ), new Swiper(".rest-second", {
        //     slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
        //         nextEl: ".restaurant .item.second .swiper-button-next", prevEl: ".restaurant .item.second .swiper-button-prev"
        //     }
        //     , breakpoints: {
        //         992: {
        //             slidesPerView: 2
        //         }
        //         , 767: {
        //             slidesPerView: 1
        //         }
        //     }
        // }
        // ), new Swiper(".rest-third", {
        //     slidesPerView:3, loop:!0, spaceBetween:0, navigation: {
        //         nextEl: ".restaurant .item.third .swiper-button-next", prevEl: ".restaurant .item.third .swiper-button-prev"
        //     }
        //     , breakpoints: {
        //         992: {
        //             slidesPerView: 2
        //         }
        //         , 767: {
        //             slidesPerView: 1
        //         }
        //     }
        // }
        );
        jQuery("a.top").click(function(e) {
            e.preventDefault(), jQuery("html, body").animate( {
                scrollTop: 0
            }
            , 600)
        }
        ), jQuery(".hamb").click(function(e) {
            e.preventDefault(), jQuery(".header__nav").hasClass("opened")?(jQuery(".header__nav").removeClass("opened"), jQuery(".header__nav").slideUp(400)): (jQuery(".header__nav").addClass("opened"), jQuery(".header__nav").slideDown(400))
        }
        )
    }),
jQuery(window).on("load", function() {
    jQuery("#status").fadeOut(), jQuery("#preloader").delay(350).fadeOut("slow"), jQuery("body").delay(350).css( {
        overflow: "visible"
    })
});

$(function(){
  /* Read More Buttons */
  var temp = true;
  $(".read-more").click(function(){
    $("#" + $(this).data('id')).toggle('fast');
    if(temp) {
      temp = false;
      $(this).html("Read Less");
    }
    else {
      temp = true;
      $(this).html("Read More");
    }
  });
  /* Read More Buttons */

  /* Magnific Popup */
  $('.image-popup').magnificPopup({
    type: 'image',
    closeBtnInside: false,
    closeOnContentClick: true,
    image: {
      verticalFit: false
    }
  });

  $('.inline-popup').magnificPopup({
    type: 'inline',
    closeBtnInside: false
  });

  $('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		}
	});

  $('.popup-gallery2').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		}
	});

  $('.popup-gallery3').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		}
	});

  $('.popup-gallery4').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
    image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title');
			}
		}
	});

  /* Popup gallery for events place - occasions START */
  <?php
  # Poro-point
  if (is_page(653)) {
    $ctr = 11;
    foreach (get_field('occassions', 653) as $value) {
  ?>

    $('.popup-gallery<?php echo $ctr++; ?>').magnificPopup({
  		delegate: 'a',
  		type: 'image',
  		tLoading: 'Loading image #%curr%...',
  		mainClass: 'mfp-img-mobile',
  		gallery: {
  			enabled: true,
  			navigateByImgClick: true,
  			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
  		},
      image: {
  			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
  			titleSrc: function(item) {
  				return item.el.attr('title');
  			}
  		}
  	});

  <?php
    }
  }
  ?>

  <?php
  # Rizal
  if (is_page(655)) {
    $ctr = 11;
    foreach (get_field('occassions', 655) as $value) {
  ?>

    $('.popup-gallery<?php echo $ctr++; ?>').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          return item.el.attr('title');
        }
      }
    });

  <?php
    }
  }
  ?>
  /* Popup gallery for events place - occasions END */

  /* Popup gallery for Restaurants START */
  <?php
  # Poro-point
  if (is_page(24)) {
    $args = array('post_type' => 'poro_restaurant');
    $the_query = new WP_Query($args);
    $ctr = 11;
    if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post();
  ?>

    $('.popup-gallery<?php echo $ctr++; ?>').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          return item.el.attr('title');
        }
      }
    });
    <?php $ctr++; endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>
  <?php
  }
  ?>

  <?php
  # Rizal
  if (is_page(39)) {
    $args = array('post_type' => 'rizal_restaurant');
    $the_query = new WP_Query($args);
    $ctr = 11;
    if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post();
  ?>

    $('.popup-gallery<?php echo $ctr++; ?>').magnificPopup({
      delegate: 'a',
      type: 'image',
      tLoading: 'Loading image #%curr%...',
      mainClass: 'mfp-img-mobile',
      gallery: {
        enabled: true,
        navigateByImgClick: true,
        preload: [0,1] // Will preload 0 - before current, and 1 after the current image
      },
      image: {
        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
        titleSrc: function(item) {
          return item.el.attr('title');
        }
      }
    });

    <?php $ctr++; endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>
  <?php
  }
  ?>
  /* Popup gallery for Restaurants END */

  /* Magnific Popup */
});
</script>
