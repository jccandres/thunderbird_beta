var calendar_settings,
    dwh_calendar = function( jQuery ){

    var site_info = {};
    site_info.is_corpsite = false;

    var _booking = jQuery('#booking');
    var _ctabutton = jQuery('.ctabutton');
    var _select = jQuery('#select-hotel');
	var hotel_id = jQuery('input[name="hotelid"]').val();


    jQuery(document).ready(function(){

            init();

    });

    function init()
    {

        bindUIEvents();

    }

    function bindUIEvents()
    {

		/* activate datepicker */
        activateDatePicker();

		/* set initial date */
		set_now_date( new Date() );

        if( _booking ){

			/* append form action if no promocode */
			var booking_url = returnBookingUrl( hotel_id );
			attachBookingUrl( _booking, booking_url, 'action' );

			/* Promocode keyup */
			jQuery( document ).on('keyup', '.control-wrapper input[name="promocode"]', function(){

				var _this = jQuery(this);
				var _val = jQuery.trim(_this.val());

				/* check corpsite */
				var _val = _select.val() || '';
				isCorpSite( _val );

			});


			/* if corpsite calendar CTA */
			if( site_info.is_corpsite == true ){

				_select.change(function(){
					var _val = jQuery.trim(jQuery(this).val());

					/* check corpsite */
					isCorpSite( _val );

				});

				/* do not submit if empty*/
                _booking.submit(function(evt){
                    var hotelid = jQuery.trim( _select.val() );

                    if( hotelid == 'select' ){
                        evt.preventDefault();
                        alert('select a hotel');
                    }
                });

			}

		}


    }

	/* corpsite detection */
	function isCorpSite( value ){

		/* if corpsite */
		if( site_info.is_corpsite == true ){

			value = value != 'select' ? value : hotel_id;

			booking_url = returnBookingUrl( value );
			attachBookingUrl( _booking, booking_url, 'action' );
		}
		else{

			booking_url = returnBookingUrl( hotel_id );
			attachBookingUrl( _booking, booking_url, 'action' );
		}

	}

	/* Attach booking url */
	function attachBookingUrl( _booking, booking_url, attribute ){
		_booking.attr( attribute, booking_url );
	}

	/* booking url constructor */
	function returnBookingUrl( hotel_id ){

		if( hotel_id ){

      // var temp_date = new Array();
      // booking_date = jQuery("#booking_date").val();
      // temp_date = booking_date.split("-");
      // arrival = temp_date[0];
      // departure = temp_date[1];
			arrival = jQuery( "#arrival_date" );
			departure = jQuery( "#departure_date" );
			promo_code = jQuery( "#promo_code" );

			var base_url = 'https://reservations.directwithhotels.com/reservation/processDates/';
			var arrival_date = arrival.datepicker('getDate');
			var departure_date = departure.datepicker('getDate');
			var booking_url = base_url;
			var promocode_url = 'https://reservations.directwithhotels.com/reservation/showRooms/';
			var format_date = 'yy-mm-dd';

			arrival_date = jQuery.datepicker.formatDate( format_date, new Date( arrival_date ));
			departure_date = jQuery.datepicker.formatDate( format_date, new Date( departure_date ));

			booking_url = booking_url + hotel_id +'/campaign';

			var promo_code_val = promo_code.val();

			/* if with promo code */
			if( promo_code_val ){

				booking_url = promocode_url + hotel_id +'/'+ arrival_date +'/'+ departure_date +'/';
				booking_url += 'en/0/0/0/0/prv/promoCode/'+ promo_code_val +'/campaign';
			}

			return booking_url;
		}

	}

	function activateDatePicker(){

		jQuery('#arrival_date').datepicker({
									dateFormat: "dd M yy",
									showOn: "both",
									buttonImageOnly: false,
									buttonText: '...',
									onSelect: update_arrival_selects,
									constrainInput: true,
									changeMonth: true,
									changeYear: true,
									showTime:false,
									showHour: false,
									showMinute: false,
									showSecond: false,
									yearRange: "0:+3",
									minDate: 0
								});

        jQuery('#departure_date').datepicker({
									dateFormat: "dd M yy",
									showOn: "both",
									buttonImageOnly: false,
									buttonText: '...',
									onSelect: update_departure_selects,
									constrainInput: true,
									changeMonth: true,
									changeYear: true,
									showTime:false,
									showHour: false,
									showMinute: false,
									showSecond: false,
									yearRange: "0:+3",
									defaultDate: +1,
									minDate: 0
								});

	}

    /*calendar functions*/
    function update_departure_selects(date) {
        var date = jQuery('input[name=departure]').datepicker('getDate');
        jQuery('input[name=departure]')
            .datepicker( "setDate", jQuery.datepicker.formatDate('dd M yy', new Date(date)))
            .datepicker('hide');

		/* update booking url */
		var _val = _select.val() || '';
		isCorpSite( _val );
    }

    function update_arrival_selects(date) {
        var date = jQuery('input[name=arrival]').datepicker('getDate');
        var nextday = date.getTime() + (1000*60*60*24);
        var mindate = new Date(nextday);

		jQuery('input[name=arrival]')
            .datepicker("setDate", jQuery.datepicker.formatDate('dd M yy', new Date(date)))
            .datepicker('hide');

		jQuery('input[name=departure]')
            .datepicker('option', 'minDate', mindate)
            .datepicker("setDate", jQuery.datepicker.formatDate('dd M yy', new Date(nextday)));

		/* update booking url */
		var _val = _select.val() || '';
		isCorpSite( _val );

    }

    function set_now_date( date ) {

            var nextday = date.getTime() + (1000*60*60*24);
            var mindate = new Date(nextday);
            jQuery('input[name=arrival]')
                .datepicker("setDate", jQuery.datepicker.formatDate('dd M yy', new Date(date)))
                .datepicker('hide');

            jQuery('input[name=departure]')
                .datepicker('option', 'minDate', mindate)
                .datepicker( "setDate", jQuery.datepicker.formatDate('dd M yy', new Date(nextday)));

    }


    function update_onclick_event( _select , _val , _onclick_event  )
	{
		var output = "";
		_select.find('option').each(function() {

			if( $(this).val()!='select' )
			{
				var search_str = _onclick_event.search( $(this).val() );

				if( search_str != -1 )
				{
					output = _onclick_event.replace( $(this).val() , _val );
				}
			}
	    });

		return output;
	}


    return {

        'init'                      : init,
        'calendar_settings'         : calendar_settings,
        'bindUIEvents'             	: bindUIEvents,
        'update_departure_selects' 	: update_departure_selects,
        'update_arrival_selects'    : update_arrival_selects,
        'set_now_date'              : set_now_date ,
        'update_onclick_event'    	: update_onclick_event

    };


}( jQuery );
