<?php
/**
 * Footer Template for rizal
 */
?>
	<div class="second-half">
	<footer class="footer">
		<div class="container">
			<a href="#" class="top"><img src="<?php bloginfo("template_url"); ?>/assets/img/top1.png" alt=""></a>
			<div class="row">
				<div class="footer__text col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<?php echo get_field('rizal_contact_info', 88); ?>
					<a href="tel:<?php echo get_field('rizal_contact_no', 88); ?>" class="tel">
						<img src="<?php bloginfo("template_url"); ?>/assets/img/icon-tel.png" alt=""><?php echo get_field('rizal_contact_no', 88); ?>
					</a>
					<br>
					<a href="mailto:<?php echo get_field('rizal_contact_email', 88); ?>" class="email"><img src="<?php bloginfo("template_url"); ?>/assets/img/icon-email.png" alt=""><?php echo get_field('rizal_contact_email', 88); ?></a>
					<br>
					<a href="mailto:sales@thunderbird-asia.com" class="email">sales@thunderbird-asia.com</a>
					<div class="social">
						<a href="<?php echo get_field('rizal_facebook_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/fb.png" alt="">
						</a>
						<a href="<?php echo get_field('rizal_twitter_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/tw.png" alt="">
						</a>
						<a href="<?php echo get_field('rizal_instagram_link', 88); ?>">
							<img src="<?php bloginfo("template_url"); ?>/assets/img/insta.png" alt="">
						</a>
					</div>
				</div>
				<div class="footer__subscribe col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<form>
						<input type="email" name="email" class="email" placeholder="" required>
						<input type="submit" value="Subscribe" name="subscribe">
					</form>
					<div class="span2" style="padding-top:25px">
						<a href="http://www.doubledowncasino.com/?cid=3959&amp;origin=96" target="_blank"><img src="http://wpcdn.hotelhosting.co/wp-content/uploads/sites/921/2016/04/dd-icon.png" title="Thunderbird Resort Poro Point in La Union, Philippines" alt="Thunderbird Resort Poro Point in La Union, Philippines | undefined"></a><br>
						<strong><img src="http://wpcdn.hotelhosting.co/wp-content/uploads/sites/921/2016/08/icon-pdf.png" alt="Thunderbird Resort Poro Point in La Union, Philippines | " title="Thunderbird Resort Poro Point in La Union, Philippines"> &nbsp; <a href="<?php echo get_field('rizal_faq_file', 88); ?>" target="_blank">Responsible Gaming FAQs</a></strong>
					</div>
				</div>
				<div class="footer__logo col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<a href="<?php echo get_permalink(8); ?>"><img src="<?php echo get_field('main_logo', 8); ?>" alt=""></a>
				</div>
			</div>
			<nav class="footer__nav">
				<ul>
					<?php
					$menu_obj = wp_get_nav_menu_items('main-page-rizal-menu');
					foreach ($menu_obj as $obj) {
					?>

					<li><a href="<?php echo $obj->url ?>"><?php echo get_the_title($obj->object_id); ?></a></li>

					<?php
					}
					?>
				</ul>
			</nav>
			<p>&copy; 2017 Thunderbird-asia.com. All Rights Reserved</p>
		</div>
	</footer>
	</div>

	<!-- Preloader -->
    <div id="preloader">
        <div id="status">&nbsp;</div>
    </div>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
		<script type="text/javascript" src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/assets/js/swiper.min.js"></script>
    <!-- <script src="<?php bloginfo("template_url"); ?>/assets/js/scripts.js"></script> -->
		<?php echo get_template_part( 'assets/js/scripts' ); ?>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.magnific-popup.min.js"></script>
		<script type="text/javascript" src="<?php echo bloginfo('template_url'); ?>/assets/js/calendar.js?v1.1.1"></script>

		<!-- js for date range picker -->
		<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
		<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>

		<script type="text/javascript">
		$(function() {
			$('#arrival_date, #departure_date').daterangepicker({
				singleDatePicker: true,
				showDropdowns: true
			});
		});
		</script>

		<script src="https://cdn.jsdelivr.net/modernizr/2.8.3/modernizr.min.js"></script>
        <script>
        // Execute this if IE is detected.
        function msieversion() {
            var ua = window.navigator.userAgent;
            var msie = ua.indexOf("MSIE ");
            
            if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./)) {
                if ( ! Modernizr.objectfit ) {
                    $('.swiper-slide figure').each(function () {
                        var $container = $(this),
                            imgUrl = $container.find('img').prop('src');
                        if (imgUrl) {
                            $container
                                .css('backgroundImage', 'url(' + imgUrl + ')')
                                .addClass('cover');
                        }
                    });
                }
            }
            return false;
        } // End
        $(document).ready(msieversion);
        </script>
		
    </body>
</html>
