<?php
/**
 * Menu for Rizal page
 */
?>

<div class="container" style="z-index: 3;">
	<header class="header">

		<?php //if (!is_page(17)): ?>
		<div class="container">
			<div class="header__buttons pull-left">
				<a href="<?php echo get_permalink(8); ?>">HOME</a>
				<a href="#" class="btn">RESERVE NOW!</a>
				<a href="<?php echo get_permalink(43); ?>">PROMO DEALS</a>
			</div>
			<div class="header__contacts pull-right">
				<a href="tel:<?php echo get_field('rizal_contact_no', 88); ?>" class="tel"><?php echo get_field('rizal_contact_no', 88); ?></a>
				<div class="social">
					<a href="<?php echo get_field('rizal_facebook_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/fb.png" alt=""></a>
					<a href="<?php echo get_field('rizal_twitter_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/tw.png" alt=""></a>
					<a href="<?php echo get_field('rizal_instagram_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/insta.png" alt=""></a>
				</div>
			</div>
			<div class="clearfix"></div>
		</div>
		<?php //endif ?>

		<div class="header__logo" style="width: 50%; margin-left: 25%;">
			<a href="<?php echo get_permalink(17); ?>"><img src="<?php echo get_field('logo', 17); ?>" alt=""></a>
		</div>
		<a href="#" class="hamb"><img src="<?php bloginfo("template_url"); ?>/assets/img/hamb.png" alt=""></a>
		<nav class="header__nav">
			<ul>
				<!--<li>
					<a href="<?php echo get_permalink(8); ?>">Home</a>
				</li>-->
				<li class="has-subnav <?php echo (is_page(483) || is_page(487) || is_page(493) || is_page(37)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(37); ?>">Casino</a>
					<ul class="sub-nav rizal-sub">
						<li><a href="<?php echo get_permalink(483); ?>">Promo & Games</a></li>
						<li><a href="<?php echo get_permalink(487); ?>">Events</a></li>
						<li><a href="<?php echo get_permalink(493); ?>">Gaming Facilities</a></li>
						<li><a href="<?php echo get_permalink(632); ?>">Responsible Gaming</a></li>
						<li><a href="<?php echo get_permalink(637); ?>">Guest Shuttle Service</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(22)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(22); ?>">Rooms / Villas</a>
				</li>
				<li class="has-subnav <?php echo (is_page(292)) ? 'active' : ''; ?>">
					<a href="javascript:void(0);">Recreations</a>
					<ul class="sub-nav rizal-sub">
						<li><a href="<?php echo get_permalink(292); ?>">Facilities</a></li>
						<li><a href="<?php echo get_permalink(712); ?>">Fitness and Wellness</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(39)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(39); ?>">Restaurants</a>
				</li>
				<li class="has-subnav <?php echo (is_page(41)) ? 'active' : ''; ?>">
					<a href="javascript:void(0);">Events place</a>
					<ul class="sub-nav rizal-sub">
						<li><a href="<?php echo get_permalink(41); ?>">Venues</a></li>
						<li><a href="<?php echo get_permalink(655); ?>">Occasions</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(43)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(43); ?>">Promos</a>
				</li>
			</ul>
		</nav>
	</header>
</div>
