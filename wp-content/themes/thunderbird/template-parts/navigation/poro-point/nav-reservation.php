<?php
/**
 * Template for reservation menu
 */
?>

<div class="cta">
	<div class="container">
		<form id="booking" class="courier" method="POST" action="">
			<input type="hidden" name="hotelid" value="11580" />
			<h3><span class="check"></span>Best Price Guarantee<a class="tip"></a></h3>

			<div class="control-wrapper">
				<span class="calendar-label">Check In</span>
				<div class="calendar-input">
					<input class="hasDatepicker text_reserve inputDate" value="<?php echo date(); ?>" id="arrival_date" placeholder="Select Date" name="arrival" type="text">
				</div>
			</div>

			<div class="control-wrapper">
				<span class="calendar-label">Check Out</span>
				<div class="calendar-input">
					<input class="hasDatepicker text_reserve inputDate2" value="<?php echo date("m/d/Y", strtotime("+1 day")); ?>" id="departure_date" placeholder="Select Date" name="departure" type="text">

				</div>
			</div>

			<div class="control-wrapper">
				<div class="calendar-input">
					<input class="text_code" id="promo_code" name="promo_code" value="" type="text" placeholder="Enter promo code here" autocomplete="off">
				</div>
			</div>

			<div class="control-wrapper cta-button-container">
				<input class="button ctabutton" type="submit" value="Check Availability and Prices">
			</div>
		</form>
	</div>
</div>
