<?php
/**
 * Menu for Poro Point page
 */
?>
<div class="container" style="z-index: 3;">
	<header class="header">

		<?php //if (!is_page(15)): ?>
			<div class="container">
			<div class="header__buttons pull-left">
				<a href="<?php echo get_permalink(8); ?>">HOME</a>
				<a href="#" class="btn">RESERVE NOW!</a>
				<a href="<?php echo get_permalink(33); ?>">PROMO DEALS</a>
			</div>
			<div class="header__contacts pull-right">
				<a href="tel:<?php echo get_field('poro_contact_no', 88); ?>" class="tel"><?php echo get_field('poro_contact_no', 88); ?></a>
				<div class="social">
					<a href="<?php echo get_field('poro_facebook_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/fb.png" alt=""></a>
					<a href="<?php echo get_field('poro_twitter_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/tw.png" alt=""></a>
					<a href="<?php echo get_field('poro_instagram_link', 88); ?>"><img src="<?php bloginfo("template_url"); ?>/assets/img/insta.png" alt=""></a>
				</div>
			</div>
			<div class="clearfix"></div>
			</div>
		<?php //endif ?>

		<div class="header__logo" style="width: 50%; margin-left: 25%;">
			<a href="<?php echo get_permalink(15); ?>"><img src="<?php echo get_field('logo', 15); ?>" alt=""></a>
		</div>
		<a href="#" class="hamb"><img src="<?php bloginfo("template_url"); ?>/assets/img/hamb.png" alt=""></a>
		<nav class="header__nav">
			<ul>
				<!--<li>
					<a href="<?php echo get_permalink(8); ?>">Home</a>
				</li>-->
				<li class="has-subnav <?php echo (is_page(480) || is_page(485) || is_page(491) || is_page(35)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(35); ?>">Casino</a>
					<ul class="sub-nav">
						<li><a href="<?php echo get_permalink(480); ?>">Promo & Games</a></li>
						<li><a href="<?php echo get_permalink(485); ?>">Events</a></li>
						<li><a href="<?php echo get_permalink(491); ?>">Gaming Facilities</a></li>
						<li><a href="<?php echo get_permalink(630); ?>">Responsible Gaming</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(19)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(19); ?>">Rooms / Villas</a>
				</li>
				<li class="has-subnav <?php echo (is_page(31)) ? 'active' : ''; ?>">
					<a href="javascript:void(0);">Recreations</a>
					<ul class="sub-nav">
						<li><a href="<?php echo get_permalink(31); ?>">Facilities</a></li>
						<li><a href="<?php echo get_permalink(710); ?>">Fitness and Wellness</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(24)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(24); ?>">Restaurants</a>
				</li>
				<li class="has-subnav <?php echo (is_page(26)) ? 'active' : ''; ?>">
					<a href="javascript:void(0);">Events place</a>
					<ul class="sub-nav">
						<li><a href="<?php echo get_permalink(26); ?>">Venues</a></li>
						<li><a href="<?php echo get_permalink(653); ?>">Occasions</a></li>
					</ul>
				</li>
				<li class="has-subnav <?php echo (is_page(29) || is_page(647)) ? 'active' : ''; ?>">
					<a href="javascript:void(0);">Golf club</a>
					<ul class="sub-nav">
						<li><a href="<?php echo get_permalink(647); ?>">Club Membership</a></li>
						<li><a href="<?php echo get_permalink(29); ?>">Club Rates</a></li>
					</ul>
				</li>
				<li class="<?php echo (is_page(33)) ? 'active' : ''; ?>">
					<a href="<?php echo get_permalink(33); ?>">Promos</a>
				</li>
			</ul>
		</nav>
	</header>
</div>
