<?php
/* Template Name: Poro Point Landing Page */
get_header("poro");
while(have_posts()): the_post();
?>

<style type="text/css">
.promo-slider {
	height: 459px;
}
.promo-slider .swiper-wrapper .swiper-slide {
	width: 456px;
}
.promo-slider .swiper-wrapper .swiper-slide img {
	width: 99%;
	height: 99%;
	object-fit: cover;
}
.article .container .row .image {
	height: 397px;
}
.article .container .row .image img {
	height: 99%;
	width: 99%;
	object-fit: cover;
}
</style>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

	<!-- First Panel START -->
	<div class="article with-line">
		<div class="container">
			<h2><?php echo get_field('first_panel_main_header'); ?></h2>
			<div class="row">
				<div class="text col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<h3><?php echo get_field('first_panel_header'); ?></h3>
					<h4><strong><?php echo get_field('first_panel_sub_header'); ?></strong> <br><?php echo get_field('first_panel_sub_header_location'); ?></h4>
					<?php echo get_field('first_panel_description'); ?>
				</div>
				<div class="image col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<a href="<?php echo get_field('first_panel_image'); ?>" class="image-popup">
						<img src="<?php echo get_field('first_panel_image'); ?>" alt="">
					</a>
				</div>
			</div>
		</div>
	</div>
	<!-- First Panel END -->

	<!-- Second Panel START -->
	<div class="promo">
		<div class="container">
			<div class="row">
				<div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
					<?php echo get_field('second_panel_description'); ?>
				</div>
				<div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
					<div class="swiper-container promo-slider">
						<div class="swiper-wrapper">
							<?php
							foreach (get_field("panel") as $value) {
								?>

								<div class="swiper-slide">
									<a href="<?php echo ($value['link_type'] == 'External Link') ? $value['link'] : $value['page_link']; ?>">
										<img src="<?php echo $value['image'] ?>" alt="">
									</a>
								</div>

								<?php
							}
							?>

							<div class="swiper-slide">
								<img src="img/promo-image1.jpg" alt="">
							</div>

						</div>
					</div>
				</div>
			</div>
			<div class="swiper-scrollbar"></div>
        </div>
	</div>
	<!-- Second Panel END -->

	<!-- Third Panel START -->
	<div class="article">
		<div class="container">
			<div class="row">
				<div class="text col-lg-12 text-center">
					<h3><?php echo get_field('third_panel_header'); ?></h3>
					<?php echo get_field('third_panel_description'); ?>
					<?php
						// $string = get_field('third_panel_description');
						// echo substr($string, 0, strpos($string, "\n")); ?>
						<!-- <div id="third-panel" style="display: none;"><?php //echo substr($string, strpos($string, "\n"), strlen($string)); ?></div> -->
					<!-- <a href="javascript:void(0);" data-id="third-panel" class="btn read-more">Read more</a> -->
				</div>
			</div>
		</div>
	</div>
	<!-- Third Panel END -->

</div>

<div class="second-half">

	<!-- Topics Start -->
	<?php
	$args = array('post_type' => 'poro_point_topics');
	$the_query = new WP_Query($args);
	$ctr = 0;
	if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>
	<?php if ($ctr++ % 2 == 0): ?>
		<div class="article">
			<div class="container">
				<div class="row">
					<div class="text col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<!-- <div class="spacing-80"></div> -->
						<h3><?php echo get_the_title(); ?></h3>
						<p><?php echo apply_filters( 'the_content', get_the_content() ); ?></p>
						<a href="<?php echo get_field('page_link'); ?>" class="btn">Read more</a>
					</div>
					<div class="image col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<a href="<?php echo get_the_post_thumbnail_url(); ?>" class="image-popup">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						</a>
					</div>
				</div>
			</div>
		</div>
	<?php else: ?>
		<div class="article">
			<div class="container">
				<div class="row">
					<div class="image left col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<a href="<?php echo get_the_post_thumbnail_url(); ?>" class="image-popup">
							<img src="<?php echo get_the_post_thumbnail_url(); ?>" alt="">
						</a>
					</div>
					<div class="text col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<!-- <div class="spacing-80"></div> -->
						<h3><?php echo get_the_title(); ?></h3>
						<p><?php echo apply_filters( 'the_content', get_the_content() ); ?></p>
						<a href="<?php echo get_field('page_link'); ?>" class="btn">Read more</a>
					</div>
				</div>
			</div>
		</div>
	<?php endif ?>
	<?php endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>

	<!-- <div class="article">
		<div class="text-center">
			<a href="<?php echo get_permalink(188); ?>" class="btn">View more topics</a>
		</div>
	</div> -->
	<!-- Topics END -->

	<!-- Location START -->
	<article class="location">
		<div class="line"></div>
		<div class="container">
			<article>
				<h4>How to Get there</h4>
				<?php echo get_field('directions'); ?>
				<p><a href="<?php echo get_field('direction_map'); ?>" class="btn">VIEW MAP</a></p>
			</article>
			<aside>
				<a href="<?php echo get_field('direction_image'); ?>" class="image-popup">
					<img src="<?php echo get_field('direction_image'); ?>">
				</a>
			</aside>
		</div>
	</article>
	<!-- Location END -->

</div>

<?php
endwhile;
get_footer("poro");
?>
