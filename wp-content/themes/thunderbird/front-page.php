<?php
get_header("main");
while(have_posts()): the_post();
?>

<style type="text/css">
.gallery-first {
	height: 300px;
}
.poro-menus.swiper-slide-active {
	cursor: pointer;
}
.rizal-menus.swiper-slide-active {
	cursor: pointer;
}
</style>

<!-- First Div START -->
<div class="welcome">
	<div class="container">
		<?php get_template_part( 'template-parts/navigation/front-page/nav', 'menu' ); ?>
		<div class="welcome__text">
			<div class="swiper-container top-slider">
				<div class="swiper-wrapper">
				<?php
				foreach (get_field('main_div') as $value) {
				?>

					<div class="swiper-slide">
						<?php if ($value['bg_type'] == "Image"): ?>
							<div class="sliderimg">
								<img src="<?php echo $value['bg_image']; ?>">
							</div>
						<?php else: ?>
							<video class="sliderimg" autoplay loop muted playsinline>
					    	<source src="<?php echo $value['bg_video']; ?>" type=video/mp4>
					    </video>
						<?php endif; ?>



						<aside>
							<h2><?php echo $value['main_div_header'] ?></h2>
							<?php echo $value['main_div_content']; ?>
							<span>discover</span>
							<div class="buttons">
								<a href="<?php echo get_permalink(15); ?>" class="btn">Poro point</a>
								<a href="<?php echo get_permalink(17); ?>" class="btn">Rizal</a>
							</div>
						</aside>
					</div>

				<?php
				}
				?>

				</div>
				<div class="swiper-pagination"></div>
			</div>
			<div class="swiper-button-next"></div>
			<div class="swiper-button-prev"></div>
		</div>
	</div>
</div>
<!-- First Div END -->

<!-- Poro Point Div START -->
<section class="description">
	<div class="container">
		<div class="row">
			<div class="text">
				<div class="inner">
					<a href="<?php echo get_permalink(15); ?>"><img src="<?php echo get_field('poro_point_logo'); ?>" alt=""></a>
					<aside>
						<h2><?php echo get_field('poro_point_header'); ?></h2>
						<?php echo get_field('poro_point_content'); ?>
					</aside>
				</div>
			</div>
			<div class="gallery ">
				<div class="inner first">
					<div class="swiper-container gallery-first">
						<div class="swiper-wrapper">
							<?php
							$menu_obj = wp_get_nav_menu_items('main-page-poro-point-menu');
							foreach ($menu_obj as $obj) {
							?>

							<div class="swiper-slide poro-menus" data-url="<?php echo $obj->url; ?>">
								<h4><?php echo get_the_title($obj->object_id); ?></h4>
								<figure>
									<img src="<?php echo get_the_post_thumbnail_url($obj->object_id); ?>" alt="">
									<div></div>
								</figure>
							</div>

							<?php
							}
							?>
						</div>

						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>

				</div>
			</div>
		</div>
		<a href="<?php echo get_permalink(15); ?>" class="btn">Explore Poro Point</a>
	</div>
</section>
<!-- Poro Point Div END -->

<div class="divider"></div>

<!-- Rizal Div START -->
<section class="description">
	<div class="container">
		<div class="row">
			<div class="text">
				<div class="inner">
					<a href="<?php echo get_permalink(17); ?>"><img src="<?php echo get_field('rizal_logo'); ?>" alt=""></a>
					<aside>
						<h2><?php echo get_field('rizal_header'); ?></h2>
						<?php echo get_field('rizal_content'); ?>
					</aside>
				</div>
			</div>
			<div class="gallery ">
				<div class="inner first">
					<div class="swiper-container gallery-first">
						<div class="swiper-wrapper">
							<?php
							$menu_obj = wp_get_nav_menu_items('main-page-rizal-menu');
							foreach ($menu_obj as $obj) {
							?>

							<div class="swiper-slide rizal-menus" data-url="<?php echo $obj->url; ?>">
								<h4><?php echo get_the_title($obj->object_id); ?></h4>
								<figure>

									<img src="<?php echo get_the_post_thumbnail_url($obj->object_id); ?>" alt="">
									<div></div>
								</figure>
							</div>

							<?php
							}
							?>
						</div>

						<div class="swiper-button-next"></div>
						<div class="swiper-button-prev"></div>
					</div>

				</div>
			</div>

		</div>
		<a href="<?php echo get_permalink(17); ?>" class="btn">Explore Rizal</a>
	</div>
</section>
<!-- Rizal Div END -->


<?php
endwhile;
get_footer("main");
?>

<script type="text/javascript">
	$(function(){
		$(".poro-menus").on("click", function(){
			window.location.href = $(this).data("url");
		});

		$(".rizal-menus").on("click", function(){
			window.location.href = $(this).data("url");
		});
	});
</script>
