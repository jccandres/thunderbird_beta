<?php
/* Template Name: Rizal Casino */
get_header("rizal");
while(have_posts()): the_post();
?>
<style type="text/css">
.casino__elements .row .banner-promo {
  height: 376px;
}
.promo .container .row .images .swiper-container .swiper-wrapper .swiper-slide {
  width: 684px;
  height: 459px;
}
.promo .container .row .images .swiper-container .swiper-wrapper .swiper-slide img {
  height: 99%;
  width: 99%;
  object-fit: cover;
}
.casino__elements .row .banner-facilities {
  width: 390px;
  height: 256px;
}
.casino__elements .row .banner-facilities img {
  width: 99%;
  height: 99%;
  object-fit: cover;
}
</style>

    <div class="first-half">
        <div class="welcome">

            <?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

            <img src="<?php echo get_field('header_image'); ?>" alt="">

            <?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

        </div>

        <div class="casino">
            <div class="container">
                <div class="title">
                    <h2><?php echo get_field('header'); ?></h2>
                    <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
                </div>

                <!-- Casino Promo & Games START -->
                <div class="casino__elements">
                    <div class="heading">
                        <h3><?php echo get_field('promo_header', 483); ?></h3>
                        <p><?php echo get_field('promo_sub_header', 483); ?></p>
                    </div>
                    <div class="row">
                        <?php
                        $ctr = 0;
                        foreach (get_field('promo_images', 483) as $value) {
                        ?>

                        <div class="banner banner-promo col-lg-6 col-md-6 col-sm-6 col-xs-12 popup-gallery">
                            <a href="<?php echo $value['images']; ?>">
                              <img src="<?php echo $value['images']; ?>" alt="">
                            </a>
                        </div>

                        <?php
                        $ctr++;
                        if ($ctr >= 4) {
                          break;
                        }
                        }
                        ?>
                    </div>
                    <div class="more">
                        <a href="<?php echo get_permalink(483); ?>">VIEW MORE</a>
                    </div>
                </div>
                <!-- Casino Promo & Games END -->

                <!-- Casino Events START -->
                <div class="promo right casino-page">
                    <div class="container">
                        <div class="row">
                            <div class="text col-lg-3 col-md-3 col-sm-4 col-xs-12">
                                <h2><?php echo get_field('events_header', 487); ?></h2>
                                <p><?php echo get_field('events_sub_header', 487); ?></p>
                            </div>
                            <div class="images col-lg-9 col-md-9 col-sm-8 col-xs-12">
                                <div class="swiper-container events-slider">
                                    <div class="swiper-wrapper">
                                        <?php
                                        foreach (get_field('events_images', 487) as $value) {
                                        ?>

                                        <div class="swiper-slide popup-gallery2">
                                          <a href="<?php echo $value['images']; ?>">
                                            <img src="<?php echo $value['images']; ?>" alt="">
                                          </a>
                                        </div>

                                        <?php
                                        }
                                        ?>

                                        <div class="swiper-slide">
                                            <!-- <img src="<?php echo $value['images']; ?>" alt=""> -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="swiper-scrollbar"></div>
                    </div>
                </div>
                <!-- Casino Events END -->

                <!-- Casino Gaming Facilities START -->
                <div class="casino__elements custom">
                    <div class="heading">
                        <h3><?php echo get_field('facilities_header', 493); ?></h3>
                        <p><?php echo get_field('facilities_sub_header', 493); ?></p>
                    </div>
                    <div class="row">
                        <?php
                        $ctr = 0;
                        foreach (get_field('facilities_images', 493) as $value) {
                        ?>

                        <div class="banner banner-facilities col-lg-4 col-md-4 col-sm-4 col-xs-12 popup-gallery3">
                            <a href="<?php echo $value['images']; ?>">
                              <img src="<?php echo $value['images']; ?>" alt="">
                            </a>
                        </div>

                        <?php
                        $ctr++;
                        if ($ctr >= 9) {
                          break;
                        }
                        }
                        ?>
                    </div>
                    <div class="more">
                        <a href="<?php echo get_permalink(493); ?>">VIEW MORE</a>
                    </div>
                </div>
                <!-- Casino Gaming Facilities END -->
            </div>
        </div>

    </div>

<?php
endwhile;
get_footer("rizal");
?>
