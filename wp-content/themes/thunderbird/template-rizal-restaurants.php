<?php
/* Template Name: Rizal Restaurants */
get_header("rizal");
while(have_posts()): the_post();
$ctr = 1;
$featured_restaurants = array();
if (get_field('featured_restaurants')) {
	foreach (get_field('featured_restaurants') as $featured) {
		$featured_restaurants[$ctr++] = $featured['restaurant'];
	}
}
?>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

	</div>

	<div class="restaurant">
		<div class="container">
			<div class="title">
				<h2>Thunderbird Resorts and Casinos Rizal - <?php echo get_field('header'); ?></h2>
			</div>

			<!-- Featured Restaurants START 11-28-2017 -->
			<div class="list">

				<?php
				$args = array('post_type' => 'rizal_restaurant');
				$the_query = new WP_Query($args);
				$ctr = 1;
				$popup_gallery_ctr = 11;
				if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

					<div class="item rest-item-<?php echo $ctr; ?>">
						<div class="desc">
							<h3><?php echo get_the_title(); ?></h3>
							<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
						</div>
						<div class="photos">
							<div class="swiper-container rest rest-<?php echo $ctr; ?>">
								<div class="swiper-wrapper">
									<?php
									foreach (get_field('images') as $image) {
									?>

									<div class="swiper-slide popup-gallery<?php echo $popup_gallery_ctr; ?>">
										<a href="<?php echo $image['image']; ?>">
											<img src="<?php echo $image['image']; ?>" alt="">
										</a>
									</div>

									<?php
									}
									?>
								</div>
							</div>
							<div class="swiper-button-next"></div>
							<div class="swiper-button-prev"></div>
						</div>
					</div>

				<?php $ctr++; $popup_gallery_ctr++; endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>

			</div>
			<!-- Featured Restaurants END -->

		</div>
	</div>

</div>


<?php
endwhile;
get_footer("rizal");
?>
