<?php
/* Template Name: Poro Point Promo */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.casino__elements .row .banner-promo {
  height: 376px;
}
.casino__elements .row .banner-promo img {
  width: 99%;
  height: 99%;
  object-fit: cover;
}
</style>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

	<div class="banners">
		<div class="container">
			<div class="title">
				<h2><?php echo get_field('header'); ?></h2>
				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
			</div>

			<div class="casino__elements">
					<div class="heading">
							<h3><?php echo get_field('promo_header'); ?></h3>
							<p><?php echo get_field('promo_sub_header'); ?></p>
					</div>
					<div class="row">
							<?php
							foreach (get_field('promos') as $value) {
							?>

							<div class="banner banner-promo col-lg-6 col-md-6 col-sm-6 col-xs-12 popup-gallery">
									<a href="<?php echo $value['image']; ?>">
										<img src="<?php echo $value['image']; ?>" alt="">
									</a>
							</div>

							<?php
							}
							?>
					</div>
			</div>

		</div>
	</div>

</div>


<?php
endwhile;
get_footer("poro");
?>
