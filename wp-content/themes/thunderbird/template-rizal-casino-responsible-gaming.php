<?php
/* Template Name: Rizal Casino Responsible Gaming */
get_header("rizal");
while(have_posts()): the_post();
?>
<style>
.casino .title p {
  font-family: FuturaLight;
  font-size: 20px;
}
.casino .title ul {
  font-size: 18px;
}
.casino .title ol, .casino .title ul {
  font-size: 18px;
}
.casino .title ol {
  font-family: FuturaT_Bold;
  font-size: 20px;
}
</style>

<div class="first-half">
  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image'); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>

  </div>

  <div class="casino">

    <div class="container">
      <div class="title">
          <h2><?php echo get_field('header'); ?></h2>
          <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
      </div>

    </div>

  </div>

</div>


<?php
endwhile;
get_footer("rizal");
?>
