<?php
/* Template Name: Poro Point Golf Club Membership */
get_header("poro");
while(have_posts()): the_post();
?>

<div class="first-half">

  <div class="welcome">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

      <img src="<?php echo get_field('header_image'); ?>" alt="">

      <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

  </div>

  <div class="casino">
      <div class="container">
        <div class="title">
  				<h2><?php echo get_field('header') ?></h2>
  				<?php echo apply_filters('the_content', get_post_field('post_content')); ?>
  			</div>
      </div>
  </div>

</div>

<?php
endwhile;
get_footer("poro");
?>
