<?php
/* Template Name: Poro Point Events Place - Occassions */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.rest {
	/*height: 467px!important;*/
}
.rest .swiper-wrapper .swiper-slide img {
	width: 99%;
	height: 99%;
	object-fit: cover;
}
</style>

<div class="first-half">
	<div class="welcome">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

		<img src="<?php echo get_field('header_image'); ?>" alt="">

		<?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

	</div>

  <div class="restaurant">

		<div class="container">
			<div class="title">
				<h2>Thunderbird Resorts and Casinos Poro Point - <?php echo get_field('header'); ?></h2>
			</div>

      <!-- Main Content -->
      <div class="list">
        <?php
        $ctr = 1;
				$popup_gallery_ctr = 11;
        foreach (get_field('occassions') as $occassion) {
        ?>

        <div class="item rest-item-<?php echo $ctr; ?>">
            <div class="desc">
                <h3><?php echo $occassion['title']; ?></h3>
                <?php echo $occassion['description']; ?>
            </div>
            <div class="photos">
                <div class="swiper-container rest rest-<?php echo $ctr; ?>">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ($occassion['images'] as $value) {
                        ?>

                        <div class="swiper-slide popup-gallery<?php echo $popup_gallery_ctr; ?>">
													<a href="<?php echo $value['image'] ?>">
                            <img src="<?php echo $value['image']; ?>" alt="">
													</a>
                        </div>

                        <?php
                        }
                        ?>
                    </div>
                </div>
                <div class="swiper-button-next"></div>
                <div class="swiper-button-prev"></div>
            </div>
        </div>

        <?php
        $ctr++; $popup_gallery_ctr++;
        }
        ?>

      </div>
      <!-- Main Content END -->

    </div>

  </div>
</div>

<?php
endwhile;
get_footer("poro");
?>
