<?php
/* Template Name: Poro Point Rooms */
get_header("poro");
while(have_posts()): the_post();
?>
<style type="text/css">
.rooms__list .item {
	height: 539px;
}
.rooms__list .item img {
	height: 100%;
	object-fit: cover;
}
</style>

    <div class="first-half">
        <div class="welcome">

            <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'menu' ); ?>

            <img src="<?php echo get_field('header_image'); ?>" alt="">

            <?php get_template_part( 'template-parts/navigation/poro-point/nav', 'reservation' ); ?>

        </div>

        <div class="rooms">
            <div class="container">
                <h2><?php echo get_field('header'); ?></h2>
                <div class="text">
                    <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
                </div>
                <div class="benefits">
                    <div class="item">
                        <?php echo get_field('features_and_amenities_first_box'); ?>
                    </div>
                    <div class="item">
                        <?php echo get_field('features_and_amenities_second_box'); ?>
                    </div>
                </div>
                <div class="rooms__list">

                	<?php
					$args = array('post_type' => 'poro_point_rooms');
					$the_query = new WP_Query($args);
					if ( $the_query->have_posts() ) {  while ( $the_query->have_posts() ): $the_query->the_post(); ?>

						<div class="item">
							<img src="<?php echo get_the_post_thumbnail_url(get_the_id()); ?>" alt="">
							<div class="meta room-blue">
								<div class="desc">
									<?php echo apply_filters('the_content', get_post_field('post_content', get_the_id())); ?>
								</div>
								<span><?php echo get_the_title(); ?></span>
								<a href="<?php echo get_permalink(); ?>" style="margin-bottom: 10px;">Room Details</a>
								<a href="#">Check Availability and Prices</a>
							</div>
						</div>

					<?php endwhile; wp_reset_postdata(); } else { /** no posts found **/ } ?>

                </div>
            </div>
        </div>

    </div>

<?php
endwhile;
get_footer("poro");
?>
