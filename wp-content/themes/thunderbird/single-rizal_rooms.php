<?php
get_header("rizal");
while (have_posts()): the_post();
?>
<style>
.description .gallery .swiper-container .swiper-wrapper .swiper-slide a {
  position: relative;
}

.description .gallery .swiper-container {
	height: 420px;
}
.description .gallery .swiper-slide img {
	width: 99%;
	height: 99%;
}
</style>

<div class="first-half">
    <div class="welcome">
      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'menu' ); ?>
      <img src="<?php echo get_field('header_image', 22); ?>" alt="">
      <?php get_template_part( 'template-parts/navigation/rizal/nav', 'reservation' ); ?>
    </div>

    <div class="rooms">
        <div class="container">
          <h2><?php echo get_the_title(); ?></h2>
          <div class="text">
              <?php echo apply_filters('the_content', get_post_field('post_content')); ?>
          </div>
        </div>
    </div>

    <section class="description">
      <div class="container">
        <div class="gallery ">
          <div class="inner first">
            <div class="swiper-container gallery-first">
              <div class="swiper-wrapper">
                <?php
                $rooms = get_field("images");
                foreach ($rooms as $room) {
                ?>

                <div class="swiper-slide poro-menus">
                  <a href="<?php echo $room['image']; ?>" class="image-popup">
                    <img src="<?php echo $room['image']; ?>" alt="">
                  </a>
                </div>

                <?php
                }
                ?>
              </div>

              <div class="swiper-button-next"></div>
              <div class="swiper-button-prev"></div>
            </div>

          </div>
        </div>

      </div>
    </div>



</div>

<?php endwhile;
get_footer("rizal");
?>
