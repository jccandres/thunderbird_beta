<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'thunderb_hotel');

/** MySQL database username */
define('DB_USER', 'thunderb_user');

/** MySQL database password */
define('DB_PASSWORD', 'aNUH-yQrnNp;');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'K3dkd+O/|rtQ X?Q#ab}_@)LD:K9P_XvO?giIse1ooy~<>G~6v*?Ge*QF]!$x;,G');
define('SECURE_AUTH_KEY',  ':jH_+m36w[09AX(@*3oyL[{;P+R]eUWDtaxH6}ff-v7)z(~?,vZ2+y}YD6URb_?M');
define('LOGGED_IN_KEY',    '*bFKAIr9)wNu^ l>^3_Ln22s=I?n2~K4>Pp6NJjc3Br{eN9,VXWN)n9@^R7Bh+j*');
define('NONCE_KEY',        'Kt~H^.}@o<J5iVm:$nqnY0COa3pV?_cY]$BEy;.RQ!~ fw=fuoz:=!2Ca9b2Q4r.');
define('AUTH_SALT',        'C`9Zm;H=2Ah5j><F9W<9]A`2A)XuZz>oSEpCyXYggF,M3s$#b<BFR%3w+1ZdPw!F');
define('SECURE_AUTH_SALT', 'v6)ImhzpNz:oH%Kw,P,jTul<8}z:AH1t{T-aV,*voX!E( _&7.$1;|OLqy.it.ER');
define('LOGGED_IN_SALT',   'SaX_ci(W8C<}y:/5=Y|;?L*_fm7<3-SU?3N#s$v4Z0B.Dg+lihgZ+HgQg&(mXJ8y');
define('NONCE_SALT',       'E)b-/(58XCA|yMX S^b:k/3>cgGNOh~se02u3n9|o{I8Fvc_ly*M6.6L2Y@!E~y-');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');
